---
layout: handbook-page-toc
title: All-Remote Certification
twitter_image: "/images/opengraph/all-remote.jpg"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction 

![GitLab all-remote team](/images/all-remote/GitLab-All-Remote-Zoom-Team-Tanuki.jpg){: .shadow.medium.center}

GitLab is a pioneer in the all-remote space. As the largest all-remote company in the world, we have developed a custom certification program to test and apply knowledge attained throughout the [all-remote](/company/culture/all-remote/) section of the handbook. Emerging leaders must quickly learn and deploy remote-first practices and skills while expanding their knowledge of remote management and overall remote fluency. 

Presented at a self-directed pace, the GitLab remote certificaition is designed to help [new managers](/company/culture/all-remote/being-a-great-remote-manager/) and [individual contributors](/company/culture/all-remote/getting-started/) an opportunity to master all-remote business concepts and build key skills in remote subject areas. 

Participants will gain a point of view on all-remote through [self-paced learning](/company/culture/all-remote/self-service/) and reading. 

## Certification benefits

1. Improve your remote fluency
1. Build organization, team management, and networking skills in a remote setting
1. Understand how GitLab has built and scaled the largest all-remote company
1. Develop a framework for your own organization
1. Gain full transparency into the processes, policies, and procedures GitLab uses to be successful

## Who should complete the certification?

This certification is ideal for aspiring or new managers, individual contributors, or anyone who is looking to rapidly increase their remote foundational skills and strategic perspective. The certification is also ideal for individuals who prefer to complete training at their own pace through reading comprehension and viewing video. 

## Remote Work Foundation certification criteria

![GitLab all-remote team illustration](/images/all-remote/gitlab-com-all-remote-1280x270.png){: .shadow.medium.center}

In order to attain the **Remote Work Foundation** certification from GitLab, an individual must pass all ten knowledge assessments linked below with a score of 80% or higher. For easier tracking of your completion, save the confirmation email receipts you receive after completing each knowledge assessment. 

1. [Handbook First Knowledge Assessment](/company/culture/all-remote/handbook-first-documentation/#gitlab-knowledge-assessment-handbook-first-documentation)
1. [Adopting a Self-Service and Self-Learning Mentality Knowledge Assessment](/company/culture/all-remote/self-service/#gitlab-knowledge-assessment-adopting-a-self-service-and-self-learning-mentality)
1. [Informal Communication in an All-Remote Enviornment Knowledge Assessment](/company/culture/all-remote/handbook-first-documentation/#gitlab-knowledge-assessment-handbook-first-documentation)
1. [Embracing Asynchronous Communication Knowledge Assessment](/company/culture/all-remote/asynchronous/#gitlab-knowledge-assessment-embracing-asynchronous-communication)
1. [All-Remote Meetings Knowledge Assessment](/company/culture/all-remote/asynchronous/#gitlab-knowledge-assessment-embracing-asynchronous-communication)
1. [Communicating Effectively and Responsibly Through Text](/company/culture/all-remote/effective-communication/#gitlab-knowledge-assessment-communicating-effectively-and-responsibly-through-text)
1. [Building and Reinforcing a Sustainable Culture](/company/culture/all-remote/building-culture/#gitlab-knowledge-assessment-building-and-reinforcing-a-sustainable-culture)
1. [Combating Burnout, Isolation, and Anxiety in the Remote Workplace](/company/culture/all-remote/mental-health/#gitlab-knowledge-assessment-combating-burnout-isolation-and-anxiety-in-the-remote-workplace)
1. [Remote Collaboration and Whiteboarding](/company/culture/all-remote/collaboration-and-whiteboarding/#gitlab-knowledge-assessment-remote-collaboration-and-whiteboarding)
1. [The Non-Linear Workday Knowledge Asssessment](/company/culture/all-remote/non-linear-workday/#gitlab-knowledge-assessment-non-linear-workday)

Once you have completed all ten knowledge assessments, you will be emailed an un-accredited certification that you can post on your LinkedIn profile and social media accounts. *(As of `2020-05-20`, the certificate is being designed and will be emailed upon completion.)*

## More GitLab certifications

GitLab is actively building new certifications. Learn more about [current and upcoming certifications](/handbook/people-group/learning-and-development/certifications/) within the Learning & Development handbook. 

## Questions

If you have questions, please reach out to our [Learning & Development team](/handbook/people-group/learning-and-development/) at `learning@gitlab.com`.
