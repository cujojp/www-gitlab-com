---
layout: markdown_page
title: "Remote work emergency plan: What to do (and where to start)"
twitter_image: "/images/opengraph/all-remote.jpg"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

Due to global issues concerning [COVID-19 (Coronavirus)](https://www.cdc.gov/coronavirus/2019-ncov/index.html), many employees and employers are facing a new reality: they're remote, and they’re unsure of when they’ll be able to return to the office. 

For **leaders** who are suddenly managing work-from-home teams, there are five things you can focus on right now to maximize stability. 

*(If you're an **employee** adjusting to a WFH reality, see our [worker-focused starter guide](/company/culture/all-remote/remote-work-starter-guide/).)*

## Establish a Remote Leadership Team

![GitLab all-remote illustration](/images/all-remote/gitlab-all-remote-v1-opengraph-social-1200x630.jpg){: .shadow.medium.center}

Shifting an entire division or company to remote triggers a [shockwave of change](/company/culture/all-remote/transition/). Evaluate current managers and rally a team of experts who have remote work experience, and are able to communicate nuances and serve as resources to those who will inevitably have questions. A core part of this team's role will be to document challenges in real time, transparently prioritize those challenges, and assign DRIs ([directly responsible individuals](/handbook/people-group/directly-responsible-individuals/#empowering-dris-no-explanation-needed)) to find solutions. 

Executive assistants may take on a more significant role in the transition, functioning as a documentarian in meetings and aiding with internal communications cascaded to the rest of the organization.

## Establish a handbook

This will be rudimentary to start, and will serve as a [single source of truth](/company/culture/all-remote/handbook-first-documentation/) for more pressing questions. Communicate this company-wide, and update it continually with DRIs for common questions around tools and access. This can start as a single company webpage or repository in [Notion](https://www.notion.so/) or [Almanac](https://almanac.io/), and will serve you well even after the current crisis subsides. 

[GitLab uses GitLab](/solutions/gitlab-for-remote/) to build, sustain, and evolve its company handbook. GitLab is a collaboration tool designed to help people work better together whether they are in the same location or spread across multiple time zones. Originally, GitLab let software developers collaborate on writing code and packaging it up into software applications. Today, GitLab has a wide range of capabilities used by people around the globe in all kinds of companies and roles.

You can learn more at GitLab's [remote team solutions page](/solutions/gitlab-for-remote/).

One of the most sizable challenges when going remote is keeping everyone informed in an efficient way. Put concerted effort around systematically documenting important process changes in a central place to minimize [confusion and dysfunction](/handbook/values/#five-dysfunctions). 

## Establish a communications plan

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/gOp4lKSCulI" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the GitLab Unfiltered video above, Adrian Larssen sits down with GitLab CEO and co-founder Sid Sijbrandij, as well as GitLab Head of Remote Darren Murph, to discuss business and societal changes related to remote work in the wake of COVID-19 and the great remote work migration. Discover more in GitLab's [Remote Work playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc).*

Depending on team size, consider an always-on video conference room per team, where team members can linger, or come and go as they please. This simulation helps acclimation, enabling team members to embrace the shift to remote in a [less jarring way](/blog/2019/08/05/tips-for-mastering-video-calls/). It also shows intentionality around [informal communication](/company/culture/all-remote/informal-communication/) — an important element that occurs spontaneously in an office, and needs an immediate replacement in a remote setting. 

Whatever your current view on transparency, leaders should not hold back during this time. It's vital to maintain perspective through this shift. Everyone reacts to remote work differently, and not all homes are ideal [workspaces](/company/culture/all-remote/workspace/). This can (and likely will) feel jarring, and team members will expect frequent updates as leaders iterate on their communication plan in real-time. 

For a fast-boot on this front, consider replicating GitLab's public [communication guide](/handbook/communication/). 

## Minimize your tool stack

While functioning remotely, strip the tool stack down to a minimum. Google Docs, a company-wide chat tool (like Microsoft Teams or [Slack](/handbook/communication/#slack)), and [Zoom](/blog/2019/08/05/tips-for-mastering-video-calls/) are all you need to start. If your team needs access to internal systems through a VPN, ensure that everyone has easy access, and instructions on usage are clear. 

Working well remotely requires [writing things down](/company/culture/all-remote/effective-communication/). For companies who do not have an existing culture of documentation, this will prove to be the most difficult shift. Aim to funnel communication into as few places as possible to reduce silos and fragmentation. You'll want to proactively solve for mass confusion when it comes to finding things — policies, protocols, outreach mechanisms, messaging, etc. 

## Drive change

![GitLab all-remote laptop illustration](/images/all-remote/gitlab-all-remote-laptop-map-illustration.jpg){: .shadow.medium.center}

Humans are naturally resistant to change — particularly change that is forced during times of uncertainty or crisis. Leaders will have to meet this reality head-on. An all-hands approach to recognizing the new reality is advised, and is vital to empowering everyone to contribute to the success of a remote model. 

Particularly for companies with a strong "in-office experience" it is vital for leadership to recognize that the remote transition is a *process*, not a binary switch to be flipped. Leaders are responsible for embracing iteration, being open about what is and is not working, and messaging this to all employees.

Managing a remote company is much like managing any company. It comes down to **trust, communication, and company-wide support of shared goals**.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/R0AB8ZvnEIU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [video](https://youtu.be/IU2nTj6NSlQ) above, GitLab's Head of Remote shares his top tips for employees and employers who have been forced into a work-from-home scenario with [Jack Altman](https://twitter.com/jaltma), CEO of [Lattice](http://lattice.com/). Discover more in GitLab's [Remote Work playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc).*

## Additional resources

We recognize that many companies are in need of establishing baseline remote principles right away. For those who wish to dive deeper, consider studying and implementing the guides below — surfaced from GitLab's [comprehensive guide to remote work](http://allremote.info/). 

- [Pitfalls to watch out for when embracing remote](/company/culture/all-remote/what-not-to-do/)
- [Navigating the phases of remote adaptation](/company/culture/all-remote/phases-of-remote-adaptation/)
- [Transitioning a company to remote](/company/culture/all-remote/transition/)
- [How to use forcing functions to work remote-first](/company/culture/all-remote/how-to-work-remote-first/)
- [Combating burnout, isolation, and anxiety](/company/culture/all-remote/mental-health/)
- [Adopting a self-service mindset](/company/culture/all-remote/self-service/)
- [Remote Without Warning Webinar: How to adapt and thrive as a suddenly-remote company](https://youtu.be/n4ZZaE-XCVs?t=5)
- [Preparing Your Team for Remote Work Webinar](https://youtu.be/9tYEKAFgQQw?t=5)

## Is this advice any good?

![GitLab all-remote team illustration](/images/all-remote/gitlab-com-all-remote-1280x270.png){: .shadow.medium.center}

GitLab is the world's largest all-remote company. We are 100% remote, with no company-owned offices *anywhere* on the planet. We have over 1,300 team members in more than 65 countries. The primary contributor to this article ([Darren Murph](/handbook/marketing/readmes/dmurph/), GitLab's Head of Remote) has over 14 years of experience working in and reporting on colocated companies, [hybrid-remote](/company/culture/all-remote/hybrid-remote/) companies, and all-remote companies of various scale. 

Just as it is valid to [ask if GitLab's product is any good](/is-it-any-good/), we want to be transparent about our expertise in the field of remote work. 

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
