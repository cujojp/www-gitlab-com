---
layout: handbook-page-toc
title: "KPI Index"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction
{:no_toc}
1. This page is the list of [GitLab Key Performance Indicators](/handbook/ceo/kpis/) and links to their definitions in the handbook.
1. To maintain a [Single Source of Truth](/handbook/handbook-usage/#style-guide-and-information-architecture) and to organize [information by function and results](/handbook/handbook-usage/#organized-by-function-and-results), KPI and PI (Performance Indicator) definitions live only in the functional sections of the handbook as close to the work as possible. 

## KPI Development
**Note: Data Team KPI development starts with a handbook [Merge Request](/handbook/communication/#everything-starts-with-a-merge-request) describing the KPI. The Data Team will use the MR as the basis for an Issue, which will be used to track development progress and presentation in the Data Team [KPI Issue Board](https://gitlab.com/gitlab-data/analytics/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=KPI).** 

KPIs require varying amounts of data discovery and development prior to final release into the handbook with an automated KPI dashboard. Given a clear business definition and available data, KPIs can be developed and published quickly. Others may take weeks or months if the business definition is not clear, targets are not defined, or data infrastructure is not in place. In general, KPIs which cross functional boundaries, cross subject matter boundaries, uncover source data quality problems, or require new data infrastructure are the most expensive and time consuming to implement. 

### Development Workflow

1. KPI Definition in the Handbook
1. Data Discovery
1. Data Infrastructure Development
1. KPI Chart Development
1. Quality Assurance and Sign-off
1. Publish KPI Chart into the handbook

### Creating A New KPI

1. Create a merge request to add the KPI definition to the the appropriate functional page of the handbook. Define the KPI as completely as possible with [all of its parts](/handbook/ceo/kpis/#parts-of-a-kpi). 
     * The MR must be approved by the functional leader and the CEO.
     * Tag the CFO; VP, Finance; and Senior Director, Data in the MR for visibility into downstream changes that may need to occur.
1. Create a [Data Team issue](https://gitlab.com/gitlab-data/analytics/-/issues) using the *KPI Template* and fill in the details. Include a link to the handbook page if merged or MR if not.
1. The Data Team will contact you to arrange a discovery session and create a development plan.
1. Track development progress using the Data Team [KPI Issue Board](https://gitlab.com/gitlab-data/analytics/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=KPI).
1. KPI Development is considered complete once the KPI chart is published and the KPI entry on this page is tagged with the appropriate [operational status](/handbook/business-ops/data-team/kpi-index/#legend).

### Updating An Existing KPI

1. Create a merge request for the changed KPI definition in the the appropriate functional page of the handbook.
     * The merge request must be approved by the functional leader and the CEO.
     * Tag the CFO; VP, Finance; and Senior Director, Data in the MR for visibility into downstream changes that may need to occur.
     * FP&A will highlight changed KPIs in the Investor Metrics deck, along with a link to the Merge Request that describes the change.
1. Create a [Data Team issue](https://gitlab.com/gitlab-data/analytics/-/issues) using the *KPI Template* and fill in the details. Include a link to the handbook page if merged or MR if not.
1. The Data Team will contact you to arrange a discovery session and create a development plan.
1. Track development progress using the Data Team [KPI Issue Board](https://gitlab.com/gitlab-data/analytics/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=KPI).
1. KPI Development is considered complete once the KPI chart is published and the KPI entry on this page is tagged with the appropriate [operational status](/handbook/business-ops/data-team/kpi-index/#legend).

#### Special KPI Handling

The CFO will notify investors and relevant external parties if the following KPIs are changed:
- [Net Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn)
- [Gross Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn)
- [ARR](/handbook/sales/#annual-recurring-revenue-arr)

## KPI/OKR relationship

[OKRs](/company/okrs/#what-are-okrs) are what initiatives are being worked on in a quarter; things that happen every quarter are measured with KPIs. If you change a KPI, consider making it an OKR.

## Discussion about page improvements
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/R1F-Iup-5Tc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Progress

There are two goals that are tracked on this page:
*  [Phase 1](/handbook/business-ops/data-team/kpi-index/#phase-1) and
*  [Phase 2](/handbook/business-ops/data-team/kpi-index/#phase-2)

### Phase 1

The goal of Phase 1 is for each KPI to be operational in any system with a link to that data visualization or as an embedded chart in the handbook.


### Phase 2

The goal of Phase 2 is to embed a Sisense chart for each KPI that can be operationalized.
KPI must be [fully defined](/handbook/ceo/kpis/#parts-of-a-kpi) with the data source available in the Data warehouse for the Data Team to prioritize the KPI.
This dashboard shares the progress of operationalizing KPIs in Sisense:

<iframe class="dashboard-embed" src="https://app.periscopedata.com/shared/5890b9a4-8608-4a82-a23d-635af1ad0529?embed=true" height="700"> </iframe>

## Legend

The icons below are relevant for [Phase 1](/handbook/business-ops/data-team/kpi-index/#phase-1) and can be assigned by anyone at GitLab.

📊 indicates that the KPI is operational and is embedded in the handbook next to the definition and shown publicly (can be Sisense or another system).

🔗 indicates that the KPI is operational and there is a link from the handbook to Sisense, GitLab, Bitergia, Grafana, or another system.
This may be because the KPI cannot be public or because it isn't possible to embed it yet.

🚧 indicates that the KPI is in a `WIP: work in progress` status estimated to be shipped in any system within the month. When using this indicator, an issue should also be linked from this page.

🐔 indicates that the KPI is unlikely to be operationalized in the near term.


## GitLab KPIs

GitLab KPIs are duplicates of goals of the reports further down this page.
GitLab KPIs are the most important indicators of company performance, and the most important KPI is IACV.

1. [IACV](/handbook/sales/#incremental-annual-contract-value-iacv) vs. plan > 1  [🔗](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6933430&udv=0) (lagging)
1. [Capital Consumption < Plan](/handbook/finance/corporate-finance-performance-indicators/#capital-consumption-vs-plan) [🔗](https://app.periscopedata.com/app/gitlab/483606/Finance-KPIs?widget=6279480&udv=0) (lagging)
1. [IACV efficiency](/handbook/sales/performance-indicators/#iacv-efficiency) > 1 [📊](https://app.periscopedata.com/app/gitlab/446004/Sales-KPIs?widget=6912064&udv=0) (lagging)
1. [Net New Business Pipeline Created ($s)](/handbook/marketing/performance-indicators/index.html#net-new-business-pipeline-created) v Plan > 1  [📊](https://app.periscopedata.com/app/gitlab/431555/Marketing-Metrics?widget=6592070&udv=0) (leading for IACV)
1. [Wider community merged MRs per release](/handbook/marketing/performance-indicators/index.html#wider-community-merged-mrs-per-release) [📊](https://app.periscopedata.com/app/gitlab/593556/Wider-Community-Dashboard?widget=7765561&udv=0) (lagging)
1. [LTV / CAC ratio](/handbook/marketing/performance-indicators/index.html#ltv-cac-ratio) > 4 🐔(lagging)
1. [Interviewee Satisfaction (ISAT)](/handbook/people-group/people-team-performance-indicators/#interviewee-satisfaction-isat) > 4.1 [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6858936&udv=820057) (lagging)
1. [Hires vs. plan](/handbook/hiring/performance_indicators/#hires-vs-plan) > 0.9 [📊](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6888845&udv=820057)
1. [12 month team member retention](/handbook/people-group/people-success-performance-indicators/#team-member-retention-rolling-12-months) > 84% [🔗](https://app.periscopedata.com/app/gitlab/482006/People-KPIs?widget=6251791&udv=904340) (lagging)
1. [Merge Requests Rate](/handbook/engineering/development/performance-indicators/#mr-rate) > 10 [📊](https://app.periscopedata.com/app/gitlab/504639/Development-KPIs?widget=6946736) (lagging)
1. [GitLab.com Availability](/handbook/engineering/infrastructure/performance-indicators/#gitlab-com-availability) > 99.95% [🔗](https://dashboards.gitlab.com/d/GTp20b1Zk/public-dashboard-splashscreen?orgId=1&from=now-30d&to=now) (lagging)
1. [SMAU](/handbook/product/metrics/#stage-monthly-active-users-smau) [🚧](https://gitlab.com/gitlab-data/analytics/-/issues/3840) (lagging)
1. [Support Satisfaction](/handbook/support/performance-indicators/#support-satisfaction-ssat) [📊](https://app.periscopedata.com/app/gitlab/463858/Engineering-KPIs?widget=5992548) (lagging)
1. [Runway](/handbook/finance/corporate-finance-performance-indicators/index.html#runway) > 12 [🔗](https://app.periscopedata.com/app/gitlab/483606/Finance-KPIs?widget=6880820&udv=0) (lagging)
1. [Product Downloads](/handbook/alliances/#product-download-by-distribution-method) [📊](https://app.periscopedata.com/app/gitlab/428908/GitLab-Downloads-by-Installation-Type?widget=5542406&udv=0) (leading for usage)

## CoST KPIs

<%= kpi_list_by_org("Chief of Staff Team") %>

## Sales KPIs

<%= kpi_list_by_org("Sales") %>

## Marketing KPIs

<%= kpi_list_by_org("Marketing") %>
          

## People Group KPIs

<%= kpi_list_by_org("People Team") %>

### People Success KPIs

<%= kpi_list_by_org("People Success") %>

### Recruiting KPIs

<%= kpi_list_by_org("Recruiting") %>

## Finance KPIs

### Corporate Financial KPIs

<%= kpi_list_by_org("Corporate Finance") %>

### Finance Team KPIs

<%= kpi_list_by_org("Finance Team") %>

## Product KPIs

<%= kpi_list_by_org("Product") %>

## Product Strategy KPIs

<%= kpi_list_by_org("Product Strategy") %>

## Engineering KPIs

<%= kpi_list_by_org("Engineering Function") %>

### Customer Support Department KPIs

<%= kpi_list_by_org("Customer Support Department") %>

### Development Department KPIs

<%= kpi_list_by_org("Development Department") %>

### Infrastructure Department KPIs

<%= kpi_list_by_org("Infrastructure Department") %>

### Quality Department KPIs

<%= kpi_list_by_org("Quality Department") %>

### Security Department KPIs

<%= kpi_list_by_org("Security Department") %>

### UX Department KPIs

<%= kpi_list_by_org("UX Department") %>

## GitLab Metrics

We share a spreadsheet with investors called "GitLab Metrics."
The GitLab Metrics Sheet should be a subset of the KPIs listed on this page.
Alternatively, the sheet may show variations or subsets of one of those KPIs, such as showing all licensed users and then licensed users by product.

## Satisfaction

We do satisfaction scope on a scale of 1 to 5 how satisfied people are with the experience.
We don’t use NPS since that cuts off certain scores and we want to preserve fidelity.
We have the following abbreviation letter before SAT, please don’t use SAT without letter before to specify it:

- C = unused since customer is ambiguous (can mean product or support, not all users are customers)
- E = unused since employee is used by other companies but not by us
- I = [Interviewee](/handbook/hiring/metrics/#interviewee-satisfaction-isat) (would you recommend applying here)
- L = [Leadership](/handbook/eba/#leadership-sat-survey) (as an executive with dedicated administrative support, how is your executive administrative support received)
- O = [Onboarding](/handbook/people-group/people-operations-metrics/#onboarding-tsat) (how was your onboarding experience)
- P = [Product](/handbook/product/metrics/#paid-net-promoter-score) (would you recommend GitLab the product)
- S = [Support](/handbook/support/#support-satisfaction-ssat) (would you recommend our support followup)
- T = Team-members (would you recommend working here)

## Retention

Since we track retention in a lot of ways, we should never refer to just "Retention" without indicating what kind of retention.
We track:

* [Net Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn)
* [Gross Retention](/handbook/customer-success/vision/#retention-and-reasons-for-churn)
* User Retention
* [Team Member Retention](/handbook/people-group/people-group-metrics/#team-member-retention)

## Working with the Data Team

Please see [the Data Team Handbook](/handbook/business-ops/data-team/).
