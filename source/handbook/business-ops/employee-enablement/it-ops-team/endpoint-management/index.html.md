---
layout: handbook-page-toc
title: "Endpoint Management at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


# What is this about?

At GitLab, we plan to use centralized laptop management for company-issued laptops. If we start doing that, we'll change this sentence. We have this page already live in the handbook so we can make many small MRs.

If you are in possession of a company-issued Apple laptop, this applies to you. This does not apply to personal laptops or mobile phones.

# Why are we doing it?

At GitLab, we are committed to ensuring the success of the business.  In order to achieve compliance with frameworks such as SOX (required as part of public company readiness), [SOC](https://www.aicpa.org/content/dam/aicpa/interestareas/frc/assuranceadvisoryservices/downloadabledocuments/trust-services-criteria.pdf), [NIST](https://nvd.nist.gov/800-53/Rev4/control/PE-20), and in preparation of [FedRAMP](https://www.fedramp.gov/assets/resources/documents/FedRAMP_Security_Assessment_Framework.pdf) and [ISO 27001](https://www.isms.online/iso-27001/annex-a-8-asset-management/), certain protections of company assets are mandated. Additionally, to meet the rigorous security requirements of enterprise customers who desire to use our service, an endpoint management solution is necessary. We have selected an endpoint management solution that will accomplish the following:

What the endpoint management solution **will not** do:

* Content filtering
* Collect, log or track personal activity (including website visits or purchases)
* Remote viewing
* Key-logging
 
What the endpoint management solution **will** do:

* Allow for software to be remotely deployed without requiring manual installation
* Maintain asset inventory of all GitLab owned devices
* Software license management
* Enable whole disk encryption
* Provide the ability to remotely wipe a device that has been lost or stolen
* Allow for the configuration of security features such as required passwords and OS updates

# Apple laptops

## Why JAMF?

We performed a proof of concept of multiple solutions and determined JAMF to be the best option due to it’s complete suite of features that meets GitLab compliance and customer requirements as well as providing end-user transparency through accessible logs.

Our list of requirements included:

* Lightweight agent with minimal compute footprint
* Ability to lock & wipe the laptop remotely
* Security management policies - Disk encryption, password strength
* Schedule OS & software installation and updates
* Receive non-compliant endpoint notifications
* SSO support
* Hardware health status (battery and harddrive health)
* Report serial number, model/type. enumerate hardware and OS version
* Enumerate installed software (+browser extensions)
* Software management
* Log data on user logins, IP address and machine info
* Allow users to see what's being collected

## What data is being collected? How can I view it?

The data collected from your company-issued Apple laptop can be viewed in XML format by accessing `~/Documents/Jamf_Data.xml` on your Apple laptop. We recommend an XML parser to view the data.

## Implementation plan

* 1st wave (2020-06-15): IT, Security
* 2nd wave (2020-06-22): Cross Functional Pilot Groups
* 3rd wave (2020-06-29): E-Group
* 4th wave (2020-07-06): People, Recruiting, Finance, Product, Marketing
* 5th wave (2020-07-13): Sales, Legal, Chief of Staff team
* 6th wave (2020-07-20): Engineering

# Linux laptops

We do not have Linux-based endpoint management in place yet. There will be a second initiative to address Linux management later in FY21.

# Windows laptops

The Windows operating system is not a supported platform at GitLab, as described in the [Internal Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/#unable-to-use-company-laptop). If you’re using a Windows laptop, please contact [IT](/handbook/business-ops/it-help/#get-in-touch) to have a company laptop shipped to you.

# Support Information

Slack: [#it-help](https://gitlab.slack.com/archives/CK4EQH50E)

# Endpoint (Laptop) Management FAQ
Please note, we will be updating this section with answers to common questions that employees have. As we move forward, this section is expected to grow significantly.

## What is an endpoint?
An endpoint is any device that is physically an endpoint on a network. These can include laptops, desktops, mobile phones, tablets, servers, and virtual environments. For the purposes of this project however, the scope is limited to Apple laptops.

## What is endpoint management? 
End-point management is used to protect the corporate network when accessed via remote devices such as laptops. Each laptop with a remote connection to the network creates a potential entry point for security threats. 

## Is this necessary?
Yes. Centralized endpoint management is common and necessary in enterprise organizations looking to achieve large scale growth, IPO, and certifications. This is an expectation of our customers to meet their standards in order to utilize our service.

### What is Jamf?
Jamf is an Apple device management solution used by system administrators to configure and automate IT administration tasks for macOS, iOS, and tvOS devices. The current project will focus solely on macOS devices 

### Why are we outsourcing our end-point management and what are the benefits?
The Jamf Pro endpoint management solution provides a lot of advantages over an open-source/build-it-yourself solution. Some of these include integration with our Single Sign-on Identity management system (Okta), Security and access profiles, and a self-service application that allows users to easily install officially supported applications. While a read-only solution would address some of these basic tenets, not everyone in the company is technical enough or motivated to manage the security of their machine. Therefore we require a solution that can be an active component in enforcing security measures. 

### Who owns and manages Jamf at GitLab?
GitLab IT Operations is the owner of Jamf and the IT Manager is the DRI.

### Is the solution's backend self-hosted or hosted/managed by Jamf?
Gitlab is a fully remote/SaaS first company, so the backend Jamf server will not be self-hosted. We will be using their SaaS service for hosting.

### Is my personal activity being monitored?
No. This is not an activity monitoring solution.

### Does this mean that you're able to view my browsing history?
No, browsing activity will neither be tracked nor monitored.

### Will remote viewing occur?
No, per policy we will not perform screen sharing. If laptop support is needed, it will be upon request with your desktop shared through Zoom.

### What about personal laptops?
Personal laptops are not in scope here since they are not issued by GitLab. If you are using a personal laptop for business purposes please ensure you comply with our [Acceptable Use Policy](../../../../../people-group/acceptable-use-policy.md#bring-your-own-device-byod) at all times. 

### Can someone Secure Shell (SSH) into my laptop?
Only the IT Team will have administrative access into Jamf, and interactive Secure Shell into user's laptops will not be done without first obtaining permission from the user.

### Who has access to the data that's being collected? Who can manage security policies? Who can trigger remote laptop wipes?
The IT Operations team has access to this data and has these permissions.
Any of the IT team can trigger a remote wipe in cases where a laptop is lost or stolen, or a team-member is off-boarded. Policy creation and management will be limited to a small group within IT Operations.

### Who ensures IT Operations is managing the tool correctly and ethically?
As with any enterprise tool, both the Security and Legal team will perform audits to ensure that Admins have the correct least access privilege and are adhering to our code of conduct when using the tool Admins that abuse the endpoint monitoring tools face disciplinary action, up to dismissal, civil/criminal prosecution, and damage claims.

### How does this align with the EU General Data Protection Regulation (GDPR)?
Endpoint management is a practice that is aligned with GDPR when done right. Jamf is GDPR compliant, you can read more about it here and here.

### Where can I view data collected from my laptop?
As outlined in the merge request, all data being collected by the Jamf agent will be listed in an XML file in each user's home directory located here ~/Documents/Jamf_Data.xml. Jamf offers wide community support, and customizability and we fully expect to take advantage of this and iterate towards more transparency.

### What will be the change, review, and socialization process for configuration changes to Jamf?

It will be no different than our current process for change management which is outlined here: https://about.gitlab.com/handbook/business-ops/business-technology-change-management/

### How much notice will be provided before a change is made to the data collection and operations of Jamf?

While we don't expect to be making any changes to our currently defined data privacy policy, should the need arise due to a request from the Security or Legal departments, that change would go through the same change management process as defined above.

### Will a user be notified that the endpoint management software is installing something? And will the user know what has been installed?

In general, all changes performed by Jamf will notify the user ahead of time and offer the user the option to defer the change in cases where the timing is inconvenient to the user. However, that deferral is limited and the user will eventually be forced to apply the update in cases where the update is related to security.

