---
layout: handbook-page-toc
title: "Product Metrics"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Product Metric Taxonomy

At GitLab, we have adopted the following taxonomy. In this page we will detail the most common definitions of product metrics. SOSMAU, Primary Product KPI, and North Star Metrics are definitionally all the same.

- **Primary Product KPI**: Often called as the [North Star Metric](https://amplitude.com/north-star) in the industry. It is a single metric for entire GitLab product.
  - Definition: a single critical rate, count, or ratio that represents our product strategy. This metric is a leading indicator that defines the relationship between the customer problems that the product team is trying to solve and sustainable, long-term business results.
- **Product KPIs**: KPIs for the product (often related to acquisition, monetization, retention, and growth). Primary Product KPI one of the Product KPIs.
- **Product and Growth Performance Indicators**: All other metrics that product team measures and monitors including category level metrics

The KPI index lists [Product KPIs](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/#product-kpis)

### Primary Product KPI : SOSMAU

Definition: The sum of all stage monthly active users ([SMAU](/handbook/product/metrics#stage-monthly-active-users-smau))

Our goal is to consistently create products and experiences that customers love and value. To measure our progress, we will instrument and dashboard several performance indicators.
We believe that product adoption is ultimately the outcome of building products that customers love and value. Therefore we have aligned on the **sum of all Stage Monthly Active Users (SOSMAU)** as our primary product KPI - a proxy metric of adoption. It is a summation of acquisition, retention, referral, churn, and growth across the product. We do not yet know what each product stage, category, or feature contribute to SMAU. By summing the usage of stages, it increases the value of multi-stage users in the final evaluation in contrast with just looking at MAU directly across GitLab.
Over time we will learn this and understand the journey towards user adoption.

Today, we are in early stages of metric definition, and instrumentation. For some stages it is unclear how we arrive at SMAU or even if SMAU is the right metric. We will continue to iterate on it.
As we gain more confidence in our metrics and how they relate to adoption, we expect to have quarterly goals around them. This data will be used to inform future investment decisions. However, this is only one of many inputs that go into making investment decisions. Thinking lower SMAU implies lower investment would be a misunderstanding of how we plan to use this metric. Often times lower SMAU results from packaging and monetization considerations. Additionally newer initiatives will have lower SMAU as it takes time for new capabilities to get adoption.


#### Why did we pick only one metric to measure such a wide product?

It's important to understand what the Primary Product KPI metric is, and how it can be used. You can read more about it in [Amplitude's North Star handbook](https://amplitude.com/north-star).

The Primary Product KPI (Referred to as North Star Metric in the industry) is an important communication device, especially when used as a framework. Preferably, every product manager, UX researcher, product designer,  engineer, and test developer in the team should be aware of the primary product KPI and there is an active discussion around the input metrics that might drive the KPI.

The ideal single metric is a leading indicator, and helps us all make decisions that are aligned in the same direction. A metric like this also increases autonomy of teams. Teams can choose their own roadmaps and take ownership of how that roadmap helps improve this metric. There will always be exceptions to this. Some teams do not have a direct way to influence this metric e.g. Enablement or Defend. We will manage exceptions with other product indicators (e.g. performance for Enablement and [SMAC](/handbook/product/metrics/index.html#stage-monthly-active-clusters-smac) for Defend).

Likely, engineers will have the best ideas to move the metrics and come up with a useful breakdown. Thus their understanding is crucial.
In addition, we expect every group to have a additional product indicators that align with the development activities.

**References**:
* [Recording of North Star Metrics Presentation](https://gitlab.zoom.us/rec/play/7J1-JOr5qWo3S9Gc4wSDA_YvW425Jv6s1yYYqPFcxU21AHQCZwGkbuYUNuqyQxTlJWyX57LH0FR1yjmQ?continueMode=true) by [Hila Qu](https://about.gitlab.com/company/team/#hilaqu)
   * [Corresponding Slide Deck](https://docs.google.com/presentation/d/14y03B2Un84QxuME0ReQOcV5-zy1oEPxNYnDrGx6jGpU/edit#slide=id.g6e0dd41cf9_1_0)


#### How does the primary KPI differ from  other product KPI's. Do they matter?

All [Product KPIs](https://about.gitlab.com/handbook/business-ops/data-team/kpi-index/#product-kpis) matter and are reviewed periodically.

### Product and Growth Performance Indicators

GitLab product is made up of [Sections](https://about.gitlab.com/handbook/product/categories/#hierarchy) that are made up of [Stages](https://about.gitlab.com/handbook/product/categories/#hierarchy) that are made up of [Groups](https://about.gitlab.com/handbook/product/categories/#hierarchy) that are made up of [Categories](https://about.gitlab.com/handbook/product/categories/#hierarchy).
Each of these have additional metrics that are immensely important to understand, manage and grow the product. 

Overall [instrumentation](https://gitlab.com/gitlab-org/telemetry/-/issues/374) and [dashboarding](https://gitlab.com/groups/gitlab-data/-/epics/97) is in progress.

In addition to the SMAU KPI, every group will invariably have other metrics that help them understand the health and growth of the product. These performance indicators can be varied and are independently chosen by the teams to reflect the specific nature of the category. They may ultimately link to the KPI and these links will ebb and flow as the category matures.

#### Selecting the right performance indicators

##### Example 1: Feature Adoption

A typical performance indicator could be the total number of times a given feature is used. This can be broken down at least in the following way:

`total feature usage = number of users using the feature x number of times each user uses the feature`

Having `number of users using the feature` and `number of times each user uses the feature` as input metrics, we can target these by development efforts, and they add up into the performance indicator. The team then has two levers to improve the performance indicator viz. increasing the number of users that use the feature, or increasing the frequency with which users user the feature. These may require different investments. It is also possible that some feature enhancements may move both levers.


##### Example 2: User Journey Funnel

Another typical approach is to consider the important events in a user's journey, like a sign-up event.
This can be broken down using the funnel that leads to sign-up, and we can have separate development effort around the funnel stages, still knowing that our aim is to move more users into the later part of the funnel.

We could use the [AARRR framework](https://about.gitlab.com/handbook/product/growth/#aarrr-framework-to-understand-our-funnel) to understnd our funnel for SMAU and SSMAU. It is a tool PMs can use to articulate and understand how do they convert aware users to customers.

##### Applying Metrics to Market Segments

A product manager can slice the metrics by a few dimensions to understand what drives the performance of that action. Some examples could be:

* Industry
* Market Size (i.e. SMB, Enterprise)
* Persona
* Revenue Bands
* New Customer, Expansion, or Renewal


##### Using Segments at GitLab

A common application of segments in GitLab is to understand what pricing tier needs product investment and targeting Enterprise deals to receive feedback from. The actions most correlated with Enterprise accounts could be selected as Performance Indicator to then encourage usage by that specific segment. The more Product Managers focus on a single metric for the Enterprise customer the more likely the solution will drive value for that user demonstrating higher return on investment.


#### Growth Performance Indicators

Growth team has additional performance indicators and frameworks to understand the user adoption funnel and optimize it to help our customers derive maximum value from GitLab product that they purchase.

[Growth](https://about.gitlab.com/handbook/product/growth/) is responsible for scaling GitLab's business value.

Growth team works on driving the following performance indicators:

##### Direct signup ARR growth rate

[Direct signup ARR growth rate](https://about.gitlab.com/handbook/product/performance-indicators/#direct-signup-arr-growth-rate) is calculated by measuring the difference in actual signup ARR between two consecutive periods (current and prior) and dividing that number by the direct signup ARR of the prior period.

##### Free to Paid ARR growth rate

[Free to Paid ARR growth rate](https://about.gitlab.com/handbook/product/performance-indicators/#free-to-paid-arr-growth-rate) is calculated by measuring the difference in actual free to paid ARR between two consecutive periods (current and prior) and dividing that number by the free to paid ARR of the prior period.


##### Gross retention rate
[Gross Retention Rate](https://about.gitlab.com/handbook/product/performance-indicators/#gross-retention) calculation is extensively explained in the [Customer Success Vision Page](https://about.gitlab.com/handbook/customer-success/vision/#retention-and-reasons-for-churn)

##### Net retention rate

[Net Retention Rate](https://about.gitlab.com/handbook/product/performance-indicators/#net-retention) calculation is extensively explained in the [Customer Success Vision Page](https://about.gitlab.com/handbook/customer-success/vision/#retention-and-reasons-for-churn)


Please note: Product KPIs are mapped 1 to 1 with our Growth teams in order to focus those teams on experiments to improve our KPIs. Additional performance indicators will be tracked and may add value, but should ultimately drive one or more KPI.

#### Product Performance Indicators

##### Stage Monthly Active Users (SMAU)
Stage Monthly Active Users is a KPI that is required for all product stages. SMAU is defined as the specified AMAU (Action Monthly Active Users) within a stage in a 28 day rolling period.

SMAU is measured for marketing stages only and therefore not applicable to other stages like Growth and Enablement.

An *ideal definition for SMAU would count unique users who performed a particular set of  actions in a given stage in a given period*, and we will iterate towards that.
The current approach was chosen for technical reasons. First, we need to consider the query performance of the usage ping (e.g. time outs). Second, this allows us to not worry about the version of an instance when comparing SMAU metrics because of changing definitions.


[SMAU Dashboard for GitLab.com SaaS](https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8131064&udv=0)


| Stage | SMAU Candidate based on usage ping | Event details | Confirmed by |
|-------|------------------------------------|---------------|--------------|
| manage | user_created |`users_created: distinct_count(::User.where(time_period), :id)` |@jeremy |
| plan | issues | `:Issue.distinct_count_by(:author_id)` | @ebrinkman |
| create | merge_requests | `:MergeRequest.distinct_count_by(:author_id)` | @ebrinkman |
| verify | ci_pipelines | `:Ci::Pipeline.distinct_count_by(:user_id)` | @jyavorska |
| package | N/A |
| secure | secure_stage_ci_jobs | `highest AMAU(user_container_scanning_jobs, user_dast_jobs, user_dependency_scanning_jobs, user_license_management_jobs, user_sast_jobs)`| @david |
| release | deployments | `:Deployment.distinct_count_by(:user_id)` | @jmeshell |
| configure | clusters_applications_helm | `:Clusters::Applications::Helm.distinct_by_user` | @kencjohnston |
| monitor | projects_prometheus_active| `:Project.with_active_prometheus_service.distinct_count_by(:creator_id)` | @kencjohnston |
| defend | N/A |

We are working to define a feature to track for SMAU purposes for the following stages:
* Package
* Defend


##### Stage Monthly Active Paid User (SMAPU)
SMAPU is defined as the paid  AMAU (Action Monthly Active Users) within a stage in a 28 day rolling period.


##### Stages per User (SpU)
Stages per user is calculated by dividing [Stage Monthly Active User (SMAU)](/handbook/product/metrics/#stage-monthly-active-users-smau) by [Monthly Active Users (MAU)](/handbook/product/metrics/#monthly-active-users-mau). 
Stages per User (SpU) is meant to capture the number of DevOps stages the average user is using on a monthly basis. 
We hope to add this metric to the [stage maturity page](/direction/maturity/), alongside number of contributions. 

The more stages someone uses the more likely they are to convert to a paying customer.
**Using an extra stage triples conversion!**
So helping our users to use more stages is something that can greatly increase [IACV](/handbook/sales/#incremental-annual-contract-value-iacv).
See [this graph in Sisense, which shows stages per namespace instead of stages per user](https://app.periscopedata.com/app/gitlab/608522/Conversion-Dashboard?widget=7985190&udv=0).

<embed width="100%" height="400" src="<%= signed_periscope_url(chart: 7985190, dashboard: 608522, embed: 'v0') %>">


##### Group Monthly Active Users (GMAU)

The number of unique users that used one *chosen* category (built by the group) in a 28 day rolling period.

Picking one category instead of all is a *recommended simplification* as we believe it may not be helpful to spend time measuring every CMAU yet. However, groups should use judgement and arrive at the right measure. We have additional performance indicators that can help inform this.

Group here refers to how GitLab internally [organizes teams](/handbook/product/categories/#hierarchy).

##### Group Monthly Active Paid Users (GMAPU)

For the purposes of this definition, lets use a new term `Up-tiered features`.  `Up-tiered features` are features that are unavailable in the free tier.

GMAPU: The number of unique paid users of the *chosen* `up-tiered feature` (built by a group) in a 28 day rolling period.
Picking one `up-tiered feature` is a *recommended simplification*. The choice of the feature is left to the judgement of each team.

Consider this made-up example. Say we have 1 million active users (800k free, 200k paid). Of the 800K free, 400K users use group features. Of the 200K paid, 70% use group features. Of the 200K paid, 20% use the up-tiered group features. GMAU is 540K, GMAPU is 40K. We recognize that there are 140K paid users that are using features from this group.

We prefer this definition as it helps us figure out how our up-tiering is working. The lower this number, the higher risk that the customer may down-tier in the future.


##### Actions
An action describes an interaction the user has within a stage. The actions that need to be tracked have to be pre-defined.

##### Action Monthly Active Users (AMAU)
AMAU is defined as the number of unique users for a specific action in a 28 day rolling period. AMAU helps to measure the success of features.

Note: There are metrics in usage ping under usage activity by stage that are not user actions and these should not be used for AMAU.
examples: 

* Groups - `:GroupMember.distinct_count_by(:user_id)`
This is the number of distinct users added to groups, regardless of activity

Other counter examples:

* ldap_group_links: `count(::LdapGroupLink)`
This is a count of ldap group links and not a user initiated action

* projects_with_packages - `:Project.with_packages.distinct_count_by(:creator_id)`
This is a setting not an action

##### Stage Monthly Active Clusters (SMAC)
In categories where there is not a directly applicable SMAU metric as the category is focused on protecting our customer's customer, other metrics need to be reviewed and applied.  An example of a group where this applies is [Defend's Container Security group](https://about.gitlab.com/handbook/product/categories/#container-security-group) where their categories are designed to protect our customers and by extension their customers who are actively interacting with our customer's production / operations environment.  Metrics such as stage monthly active clusters (SMAC) can be used in place of SMAU until it is possible to achieve an accurate metric of true SMAU which may include measurements of monthly active sessions into the customer's clusters.

##### Action Monthly Active Namespaces (AMAN)
AMAN is defined as the number of unique namespaces in which that action was performed in a 28 day rolling period.

##### Stage Monthly Active Namespaces (SMAN)
Stage Monthly Active Namespaces is a KPI that is required for all product stages. SMAN is defined as the highest AMAN within a stage in a 28 day rolling period. 

##### Feature Effect on IACV
As the most important metric in the company, Product have a defined an [Investment Thesis](/handbook/product/product-management/#investment-thesis) to measure the impact on IACV of a feature

##### Category Maturity Achievement
Percentage of category maturity plan achieved per quarter

##### Paid Net Promoter Score
Abbreviated as the PNPS acronym, please do not refer to it as NPS to prevent confusion. Measured as the percentage of paid customer "promoters" minus the percentage of paid customer "detractors" from a [Net Promoter Score](https://en.wikipedia.org/wiki/Net_Promoter#targetText=Net%20Promoter%20or%20Net%20Promoter,be%20correlated%20with%20revenue%20growth.) survey.  Note that while other teams at GitLab use a [satisfaction score](/handbook/business-ops/data-team/metrics/#satisfaction), we have chosen to use Paid Net Promoter Score in this case so it is easier to benchmark versus other like companies.  Also note that the score will likely reflect customer satisfaction beyond the product itself, as customers will grade us on the total customer experience, including support, documentation, billing, etc.


##### Monthly Active Users (MAU)
###### Self-Managed MAU
The number of unique users that performed an [event](https://docs.gitlab.com/ee/api/events.html) on a self-hosted instance within the previous 28 days.

[Dashboard](https://app.periscopedata.com/app/gitlab/425984/Month-over-Month-Overestimated-SMAU?widget=5571275&udv=688121)

###### SaaS MAU
The number of unique users that performed an [event](https://docs.gitlab.com/ee/api/events.html) on GitLab.com within the previous 28 days.

[Dashboard](https://app.periscopedata.com/app/gitlab/527913/Product-KPIs?widget=8079819&udv=0)

##### User Return Rate
Percent of users or groups that are still active between the current month and the prior month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1433)

##### Churn
The opposite of User Return Rate. The percentage of users or groups that are no longer active in the current month, but were active in the prior month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1466)

#### Other GitLab.com Only Performance Indicators

##### Active Churned User
A GitLab.com user, who is not a MAU in month T, but was a MAU in month T-1.

[Dashboard](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)

##### Active Retained User
A GitLab.com user, who is a MAU both in months T and T-1.

[Dashboard](https://app.periscopedata.com/app/gitlab/461561/GitLab.com-Health)


##### Paid User
A GitLab.com [Licensed User](https://app.periscopedata.com/app/gitlab/500504/Licensed-Users-by-Rate-Plan-Name)

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1445)

##### Paid MAU
A [paid](/handbook/finance/gitlabcom-metrics/index.html#paid-user) [MAU](/handbook/finance/gitlabcom-metrics/index.html#monthly-active-user-mau).

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1446)

##### Monthly Active Namespace (MAN)
A GitLab [Group](https://docs.gitlab.com/ee/user/group/), which contains at least 1 [project](https://docs.gitlab.com/ee/user/project/) since inception and has at least 1 [Event](https://docs.gitlab.com/ee/api/events.html) in a calendar month.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1448)

##### Active Churned Group
A GitLab.com group, which is not a MAG in month T, but was a MAG in month T-1.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1449)

##### Active Retained Namespace
A GitLab.com namespace both in months T and T-1.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1450)


##### New Namespace
A GitLab.com namespace, which is part of a paid plan, i.e. Bronze, Silver or Gold. [Free licenses for Ultimate and Gold](/blog/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/) are currently included.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1451)


##### Paid Namespace
A GitLab.com namespace, which is part of a paid plan, i.e. Bronze, Silver or Gold. [Free licenses for Ultimate and Gold](/blog/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/) are currently included.

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1452)

##### Paid MAG
A [paid](/handbook/finance/gitlabcom-metrics/index.html#paid-group) [MAG](/handbook/finance/gitlabcom-metrics/index.html#monthly-active-group-mag).

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1453)


#### Other Self-Managed Only Performance Indicators

##### Active Hosts
The count of active [Self Hosts](/pricing/#self-managed), Core and Paid, plus GitLab.com.

This is measured by counting the number of unique GitLab instances that send us [usage ping](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html).

We know from a [previous analysis](https://app.periscopedata.com/app/gitlab/545874/Customers-and-Users-with-Usage-Ping-Enabled?widget=7126513&udv=937077) that only ~30% of licensed instances send us usage ping at least once a month.

<embed width="100%" height="400px" src="<%= signed_periscope_url(chart: 7441303, dashboard: 527913, embed: 'v2') %>">

##### Product Tier Upgrade/Downgrade Rate
This is the conversion rate of customers moving from tier to tier

[Dashboard issue](https://gitlab.com/gitlab-data/analytics/issues/3084)

##### Lost instances
A lost instance of self-managed GitLab didn't send a usage ping in the given month but it was active in the previous month

[Dashboard Issue](https://gitlab.com/gitlab-data/analytics/issues/1461)

#### Other Acquisition Indicators


##### Acquisition success
Acquisition success measures how successful GitLab at integrating the new teams and technologies acquired. An acquisition is a success if it ships 70% of the acquired company's product functionality as part of GitLab within three months after acquisition. The acquisition success rate is the percentage of acquired companies that were successful. The target is 70% success rate.

##### Sourced pipeline
The sourced pipeline includes companies operating in a DevOps segment relevant for GitLab. Companies have developed or are working on product functionality which is either a new category on our roadmap or at a more advanced maturity than our current categories. Target is ongoing monitoring of at least 1000 companies.
