---
layout: handbook-page-toc
title: "TAM and Product Interaction"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

----

## Product interacting with TAMs

If you are a Product Manager or Product Designer and are seeking feedback from customers, you should consult with the TAM organization, as TAMs have direct access and regular communication with customers across all regions, tiers, use cases, and industries.

To request a meeting with a customer, open an issue in the [TAM project](https://gitlab.com/gitlab-com/customer-success/tam) and use the "Product Engagement" [issue](https://gitlab.com/gitlab-com/customer-success/tam/-/issues/new?issue) template, filling out the appropriate fields. If you have a specific customer in mind that you'd like feedback from, please share the customer name in the issue and tag the TAM assigned if you know who it is.

The TAM team gets notified via Slack whenever a new issue is opened, and they will respond in the issue with specific customers and when they are available. If you don't receive a response within a week (allowing the TAM to review with their customers), feel free to ping the [`@timtams`](https://gitlab.com/timtams) group in the issue.

## TAMs interacting with Product

### Feature Requests

If your customer has a feature request, refer to the [example of how to express the customer's interest](https://about.gitlab.com/handbook/product/how-to-engage/#a-customer-expressed-interest-in-a-feature) in an issue.

#### Calls with Product

In preparation of a call, make sure to [prepare both the customer and Product](https://about.gitlab.com/handbook/product/how-to-engage/#examples-a-customer-has-a-feature-request) in advance.

