---
layout: handbook-page-toc
title: Support Team Knowledge Areas
category: References
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Overview

The purpose of this page is to provide a reference of Support team members who feel that they are knowledgeable and willing to assist with tickets in a particular area.

Unlike the [team page](/company/team/), team members listed here may or may not be [an expert](/company/team/structure/#expert), such as [working on a bootcamp](https://gitlab.com/gitlab-com/support/support-training/-/tree/master/.gitlab/issue_templates) but not completed. Many areas however do not currently have a bootcamp, or their knowledge area is very specific, such as "GitLab.com SSO".

### When to Use

Team members should make use of existing resources before making use of this list: docs, ZenDesk, Slack (support, stage/group specific channels, questions).

If no response is forthcoming or you need to find someone to assist, you can consider pinging team members listed below.

When pinging a team member, please consider the [Support Engineer Areas of Focus](https://about.gitlab.com/handbook/support/support-engineer-responsibilities.html#support-engineer-areas-of-focus) as some team members may or may not have access to what is being inquired about.

### General Technologies and Topics

Please remember to post in the appropriate Slack channel (such as #kubernetes) and/or check the [team page](/company/team/) for non-support team members who can help as well.

Topics are in alphabetical order with team members grouped by region.

| Technology    | Region | Team Member     |
|---------------|--------|-----------------|
| Docker        | AMER | Harish <br> Cody West  <br> Caleb W. |
|               | APAC | Julian            |
| ElasticSearch | AMER | Blair  <br> Michael Lussier  <br>  JasonC <br> Eldridge|
| Git LFS       | AMER | Diana <br> Will |
|               | EMEA | Ben                |
| HA            | AMER | Aric <br> James <br> JasonC <br> Gabe |
|               | APAC | Mike |
| Kubernetes    | AMER | Harish <br> JasonC <br> Michael Lussier <br> Thiago <br> Caleb W.|
|               | APAC | Alex Tanayno <br> Julian <br> Arihant |
| Linux         | AMER | Greg <br> Keven <br> Gabe <br> Cody West <br> James Lopes <br> Will |
|               | EMEA | Ben  <br> David Wainaina             |
|               | APAC | Mike              |
| Omnibus       | AMER | Aric  <br> Diana <br> Eldridge <br> Greg <br> Harish <br> John <br> Nathan <br> Gabe <br> Cody West <br> Keven |
|               | APAC | AlexS <br> Anton <br> Weimeng <br> Mike |
| Performance   | AMER | Will <br> Cody West |

### Scripting Languages

If you need help with scripting for fixes, internal tools, or any other reason.

| Technology    | Region | Team Member     |
|---------------|--------|-----------------|
| Ruby/Rails    | AMER | placeholder |
|               | EMEA | Ronald |
|               | APAC | Wei-Meng Lee <br> Arihant |
| GO            | region | placeholder |
| JavaScript    | APAC | Wei-Meng Lee <br> Arihant |
|               | EMEA | Ronald |
| Vue.js        | APAC | Wei-Meng Lee |
| Ansible       | APAC | Wei-Meng Lee <br> Julian <br> Mike |
| Zendesk app development | EMEA | DeAndre |
| Bash          | APAC | Mike |
|               | EMEA | David Wainaina|

### Product Stages

This section is ordered by stage and group according to the [product categories page](/handbook/product/categories/#devops-stages). If no one is listed for an area, look for a Support Counterpart on the product page.

#### Manage

##### Access

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| LDAP                   | AMER | Blair <br> Diana <br> Harish <br> JasonC |
|                        | APAC | Alex Tanayno <br> Athar <br> Matthew  |
| SAML                   | AMER | Blair <br> Diana <br> JasonC |
|                        | APAC | Anton <br> Arihant|
| SSO for Groups         | AMER | Cynthia <br> Tristan <br> Arihant |

##### Import

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Project Import         | AMER | Cynthia |

#### Plan

##### Project Management

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Issue Tracking         | APAC   | Arihant |

##### Portfolio Management

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
|  | | |

##### Certify

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Service Desk | Amer | John <br> Eldridge |

#### Create

##### Source Code

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Code Owners | AMER | Tristan <br> Will |

##### Editor

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Snippets | AMER | John |
| Web IDE | AMER | John |

##### Gitaly

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Gitaly | AMER | Will |

##### Ecosystem

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| JIRA Integration | AMER | Tristan <br> Aric <br> Eldridge |

#### Verify

##### Continuous Integration

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| CI | EMEA | DeAndre <br> Ronald <br> Silvester <br> David Wainaina|
|    | AMER | Cynthia <br> Harish <br> Cody West <br> Cleveland <br> Gabe <br> Caleb W. |
|    | APAC | Alex Tanayno <br> Athar <br> Arihant|
| Jenkins Integration | AMER | Aric <br> Eldridge |

##### Runner

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| GitLab Runner          | APAC   | Alex Tanayno <br> Arihant            |
|                        | EMEA   | Ronald      |
|                        | EMEA   | Silvester   |
| Kubernetes Executor    | AMER   |             |
|                        | EMEA   |             |
|                        | APAC   |             |
| Windows                | AMER   |             |
|                        | EMEA   |             |
|                        | APAC   | Anton       |

##### Testing

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
|                        | AMER   |             |
|                        | EMEA   |             |
|                        | APAC   |             |

#### Package

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Package                | AMER   | Thiago <br> Sara <br> Caleb W. |
|                        | EMEA   |             |
|                        | APAC   |             |
| Container Registry     | AMER   |  Will       |
|                        | EMEA   |             |
|                        | APAC   |             |
| Helm Chart Registry    | AMER   |             |
|                        | EMEA   |             |
|                        | APAC   |             |
| Dependency Proxy       | AMER   |             |
|                        | EMEA   |             |
|                        | APAC   | Anton       |

#### Release

##### Progressive Delivery

| Topic                  | Region | Team Member |
|-------------|------------------------|-------------|
|  | | |

##### Release Management

| Topic                  | Region | Team Member |
|-------------|------------------------|-------------|
| Release Orchestration | Amer | JamesM |
| Secrets Management | Amer | JamesM |
| Release Evidence | Amer | JamesM |
| GitLab Pages | Amer | John <br> Keven <br> JamesM <br> Brie|

#### Configure

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
|  | | |

#### Monitor

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Metrics                | APAC   | Arihant |

##### Health

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
|  | | |

#### Secure

| Group             | Topic                  | Team Member |
|-------------------|------------------------|-------------|
| Static Analysis   | SAST                   | AMER: <br> Thiago <br> Greg |
| Dynamic Analysis | DAST                   | AMER: <br> Thiago <br> Greg |
| Composition Analysis | Dependency Scanning <br> Container Scanning <br> License Compliance | AMER: <br> Greg |

#### Defend

##### Container Security

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
|  WAF | AMER | Brie |

#### Growth

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| License and Renewals | AMER | Blair <br> Cynthia <br> David C <br> Harish <br> Keven
|                      | APAC | Jerome <br> Sameer  |
|                      | EMEA | Rene |
| Escalation Points      | EMEA | Donique |
|                        | AMER | Tom H |
|                        | APAC | Rotanak |

#### Enablement

##### Distribution

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Cloud Native / Chart   | AMER   |             |
|                        | EMEA   |             |
|                        | APAC   |             |

##### Geo

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| Geo                    | EMEA | Alin <br> Catalin |
|                        | AMER | Aric <br> Harish <br> John <br> Eldridge <br> Gabe |
|                        | APAC | Anton   |
| Backup/Restore         | EMEA |               |
|                        | AMER |               |
|                        | APAC |               |

##### Database

| Topic                  | Region | Team Member |
|------------------------|--------|-------------|
| PostgreSQL             | EMEA   | Ben         |
|                        | AMER   |             |
|                        | APAC   | Mike        |
| Database Migrations    | EMEA   |             |
|                        | AMER   |             |
|                        | APAC   |             |
