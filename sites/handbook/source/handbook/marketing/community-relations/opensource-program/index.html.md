---
layout: handbook-page-toc
title: "Open Source Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab for Open Source Program Overview
*At GitLab, our mission is to change all creative work from read-only to read-write so that everyone can contribute.* 

The [GitLab for Open Source](/solutions/open-source/) program supports GitLab's mission by empowering and enabling open source projects with our most advanced features so that they can create greater impact and amplify the contribution mindset within their spheres of influence.

This program's vision is to make GitLab the best product for Open Source projects to thrive at scale. 

Apart from actively maintaining and constantly adding value to the open source [Community Edition](/install/ce-or-ee/), GitLab makes the Enterprise Edition's top [Ultimate](/pricing/#self-managed) and [Gold](/pricing/#gitlab-com) tiers available free-of-cost to qualifying open source projects. 

### What's included?
- GitLab's top tiers ([self-hosted Ultimate and cloud-hosted Gold](/pricing/)) are [free for open source projects](/solutions/open-source/)
- 50,000 CI minutes are included for free for GitLab Gold users. Additional CI minutes can be purchased ($8 one-time fee for 1,000 extra CI minutes)
- Support can be purchased at a discount of 95% off, at $4.95 per user per month

Note: If you want to host your personal project on GitLab.com and you decide to make it publicly visible instead of private, you will automatically have access to GitLab's Gold features when you create a Free account. To access Gold features at the *group* level (to enable things like epics, roadmaps, merge requests, and other Gold features for groups), you'll need to upgrade to our paid Gold plan or you may consider applying for this GitLab for Open Source program.

### Who qualifies for the GitLab for Open Source program?

In order to qualify, your projects must meet the following program requirements: 
 1. **[OSI-approved open source license](https://opensource.org/licenses/category)** -- all of the code you write through this project must be published under an OSI-approved license. 
 1. **Non-profit** -- Your project must not seek to make a profit. Accepting donations to sustain your efforts is ok. 
 1. **Publicly visible.** -- Your source code must be publicly visible and publicly available.

See our [full terms](/terms/#edu-oss) regarding this program. For more questions, please see our [GitLab for Open Source FAQ](/solutions/open-source/#FAQ) or contact us at `opensource@gitlab.com`

### How to get started
 * Qualifying open source projects can start benefiting from self-hosted Ultimate and cloud-hosted Gold by completing a simple [application process](https://about.gitlab.com/solutions/open-source/program/).
 * Read more about the [Community Edition vs Enterprise Edition](https://about.gitlab.com/install/ce-or-ee/) to see what the best option is for your project. This page includes a way to download the Community Edition from there. 

### How to find help

#### GitLab Forum & Docs
The first place to look if you have questions is always the [GitLab Docs](https://docs.gitlab.com/) site. Our team keeps this updated so that it's the single source of truth, and it has answers to a wide variety of questions you may face.

If you can't find what you're looking for in our docs, consider asking in the [GitLab Forum](https://forum.gitlab.com/). There, you can interact with other members of our community and many GitLab employees. Even if you don't have questions, we encourage you to join the forum and meet other members of our community!

#### Submitting feedback: bugs, features, and more
In line with our Transparency value, GitLab has an [open roadmap](/direction/), so you can always see what's ahead in the product priorities. 

Your feedback helps us to build the best product possible. Whether it's a bug report, a feature request, or feedback to our company, here's a guide for [how to submit feedback](/submit-feedback/).

**Make sure to Thumbs Up issues.**
Our team looks at the numbder of `Thumbs Up` votes as an indicator for priority, so make sure you're using that feature on issues as a quick way to give us feedback. Writing detailed comments of your use-case and thorough issue desciptions is another way you can help make it easier for our team to understand your needs and respond to your questions. 

Just seach for relevant issues on the [GitLab product project](/gitlab-org/gitlab), and start giving us your feedback!

#### Public Migration Tracking Issue
If you're part of a large open source organization that is planning a migration to GitLab, please consider creating a public issue to track your progress. 

Public migration tracking issues help us understand which issues are blockers, urgent, important, and nice-to-have for your organization. It also has the added benefit of helping you get used to our workflow so that you can start giving us product feedback and help us improve the product together.

Follow these steps to create a public migration tracking issue:

 1. Create a [New Issue](https://gitlab.com/gitlab-org/gitlab/-/issues/new) in the `gitlab.com/gitlab-org/gitlab` [project](/gitlab-org/gitlab)
 1. Select the `Migrations` template in the description section where it prompts you to select a template, and click on `Apply template`
 1. Add in as much information as possible. Once finished, submit it.

 Examples of public issue trackers:
  * [KDE](https://gitlab.com/gitlab-org/gitlab/-/issues/24900)
  * [Eclipse Foundation](https://gitlab.com/gitlab-org/gitlab/-/issues/195865)
  * [Freedesktop](https://gitlab.com/gitlab-org/gitlab/-/issues/217107)

#### Support add-ons and additional services
**Priority Support**
While our GitLab for Open Source program does not include paid support, members of the program receive a 95% discount on [Priority Support](/support/). You can purchase support for $4.95 per user per month by sending us an email to opensource@gitlab.com and requesting an add-on. 

**Migration Services**
If you're looking for help with your migration, our team provides a variety of services to help. Check out our [professional services](https://about.gitlab.com/services/) page for more information. 

**Additional services**
We sometimes hear requests for services we do not provide, such as providing hosting for Community Edition instances. We've partnered with others to help expand the options for our users. Check out our [Technology Partners](/partners/) to learn more.


#### Resources and additional open source programs
There are a number of organizations that offer open source programs with discounts or free services just for open source projects. Here is a list of some of the programs we know that have been useful to pair with the GitLab for Open Source program: 

 * [AWS](https://aws.amazon.com/blogs/opensource/aws-promotional-credits-open-source-projects/) - Provides promotional credits for hosting to qualifying open source projects.
 * [Packet.io](https://www.packet.com/community/open-source/) - Packet.com provides customized options for open source organizations, based on need.

## Deep Dive into the GitLab for Open Source program
Here is more information on how the GitLab for Open Source program operates. 

### OKRs (goals for current quarter)
GitLab creates [Objectives and Key Results (OKRs)](https://en.wikipedia.org/wiki/OKR) per quarter. Here are the current OKRs for the GitLab for Open Source program:
 * [Revamp OSS Program to support growth](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/10)
 * [Build up OSS program relationships](https://gitlab.com/groups/gitlab-com/marketing/community-relations/-/epics/11)

### Where to find what we're working on
Epics, issue boards, and labels are used to track our work. For more information on the specific ones we use, please visit the [Community Relations project management page](/handbook/marketing/community-relations/project-management/). 

### Other resources

- Community Advocacy [workflow](/handbook/marketing/community-relations/community-advocacy/workflows/education-oss-startup/)
- GitLab for Open Source [repository](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/gitlab-oss/)

### Updating the GitLab for Open Source members list

We keep track and publicly display the members who have successfully applied to the GitLab for Open Source Program at https://about.gitlab.com/solutions/open-source/projects/. Whenever an application is approved, and as part of the process, the project is added to a [master list on the `/gitlab-com/marketing/community-relations/gitlab-oss` repository](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-oss/blob/master/data/oss_projects.yml).

While ideally this would be all that is required to display the members on the website, there is currently a manual step involved in transferring the file from the `/gitlab-com/marketing/community-relations/gitlab-oss` repository to the GitLab's website repository.

Follow these steps to update the GitLab for Open Source members list on the website:

1. Fetch the [latest list of GitLab for Open Source members on the `/gitlab-com/marketing/community-relations/gitlab-oss` repository](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-oss/-/raw/master/data/oss_projects.yml)
1. Copy the contents of that file into the [GitLab website's `data/oss_projects.yml` file](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/blob/master/-/data/oss_projects.yml)
1. Commit the changes and submit an MR to merge the changes into the website's master branch
1. Once the MR has been merged, the GitLab for Open Source members list will be up to date at https://about.gitlab.com/solutions/open-source/projects

<i class="fas fa-hand-point-right" aria-hidden="true" style="color: rgb(138, 109, 59)
;"></i> At this time, the [master list on the `/gitlab-com/marketing/community-relations/gitlab-oss` repository](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-oss/blob/master/data/oss_projects.yml) is only updated for applications, not for renewals. As such, it might contain data from projects who initially applied for the program but did not renew their yearly license.
{: .alert .alert-warning}

### Metrics

#### What we measure
We use the [GitLab for Open Source report](https://gitlab.my.salesforce.com/00O4M000004aCuk) on Salesforce (SFDC), along with the  [Eduoss](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/blob/master/tools/edoss/edoss.rb) script, to generate a report on: 
 * How many licenses have been issued 
 * How many seats have been issued
 * How many Gold licenses
 * How many Ultimate licenses

#### How we measure it

The steps below will generate the GitLab for Open Source Program metrics:  

1. Log into SFDC. 
1. Navigate to Reports. 
1. Open the 'GitLab for Open Source report' [SFDC report](https://gitlab.my.salesforce.com/00O4M000004aCuk)
1. Click `Run Report` to generate the report. 
1. Click `Export Details`. Choose `Unicode (UTF-8)` for the Export File Encoding and `.csv` for the Export File Format. Then click `Export` and save the file. 
1. Download the [Eduoss](https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/general/blob/master/tools/edoss/edoss.rb) script. 
1. Open a command prompt and change to the directory where you have the script and .csv file. 
1. Run the Edoss script with the exported csv file as input to generate the final csv file. 
       `./edoss.rb -i <exported_file>.csv -o <output_file>.csv`
1. Update the [GitLab for Open Source Graphs](https://docs.google.com/spreadsheets/d/19h0Era5Bh5WoG93R2wB608n9eHonPYN4ekQrHbfrI34/edit#gid=1952747469) by importing the results of the script and updating the figures. 
1. Double-check the data for consistency. It is sometimes the case that opportunities in SFDC are mistakenly named. If that is the case, fix it on the spreadsheet and reach out to a Community Advocate to fix it on SFDC.
 * Note: The processor script can provide the data in `yaml` and `json` formats in addition to .csv. 

##### Tips and Troubleshooting

If this is your first time running the report, here's a checklist of things you'll need to make sure you have first: 
 * Make sure that the Salesforce report is ready to be processed by the script: 
    * The report should have the following columns, in this order: `Campaign Name`, `Opportunity Name`, `Close Date`, `Products Purchased`, `Account Name`, `Stage`
      * If the report does not have the correct categories, you'll need to customize the report with the following steps: 
         1. Click on "customize" in the menu right above the report
         1. Search for the missing column titles, or fields (on the left menu)
         1. Drag and drop the fields into the correct order on the report
         1. When your report has all of the correct fields in the correct order, press "Save As" (top left menu). Give your report a name and description and save it to the "Marketing Reports" folder so that others at GitLab can access your report. If you save it in your personal folder, only you will be able to see it.
    * Make sure the Salesforce report has the correct filters: `Campaign Name equals "2018_OSS"` AND `Stage equals "Closed Won"EditRemove` AND `Type equals "New Business"`
      * If the report does not have the correct filters, 
         1. Click on `Customize` 
         1. Click on `Filters > Add`: `Field Filter`
         1. Create the filter as such: `Stage :: equals :: Closed Won`. 
         1. Click `OK`
         1. Repeat these steps for any other missing filters, then "Save" your report by clicking the top left menu button.

 * Before you run the script for the first time to generate metrics report, you will need to have [Ruby](https://www.ruby-lang.org/en/documentation/installation/) installed. Check to see if you have Ruby installed by running the command: 'ruby-v'. That will tell you either the version of Ruby that you're running or else that you do not have it installed. 
 * If you run into problems running the script: 
    * You may need to make the script file executable by using the following command:
       `chmod 755 edoss.rb`

## GitLab Open Source Partners
The [GitLab Open Source Partners](/solutions/open-source/partners/) program seeks to build relationships with open source projects that have thousands of community members or users (or more). Through our partnership, we aim to gain insight to help us build a better product and navigate the challenges of being an open core, for-profit company. We also aim to create greater outreach through co-marketing and special initiatives.

### Email Templates
#### Inviting project to apply
Hi [Name],

We'd like to invite you to join our [GitLab Open Source Partners](https://about.gitlab.com/solutions/open-source/partners/) program [1]. This program's aim is to feature your project in blogs, at GitLab events, and more, while also helping us engage with your community more regularly and learn more about your experience using GitLab. Since you're already part of our [GitLab for Open Source](https://about.gitlab.com/solutions/open-source/) program [2], and are a prominent open source project, we would love to have you as an open source partner. Just as an FYI, there are no membership dues for this program as it is completely free of cost. 

Please let me know if you're interested in connecting to learn more about this partnership opportunity. Feel free to grab time on my calendar for a short call: [calendly link], or if you prefer to chat more via email, we can do that too. 

Best, 
[Name]

 * [1] https://about.gitlab.com/solutions/open-source/partners/
 * [2] https://about.gitlab.com/solutions/open-source/

## Consortium Memberships and Sponsorships

### What are Consortiums?

We define Consortiums as (often open source) groups that have come together to further a technology cause. The prototypical Consortium would be the [Linux Foundation (LF)](https://en.wikipedia.org/wiki/Linux_Foundation), which is a non-profit technology consortium founded in 2000 as a merger between Open Source Development Labs and the Free Standards Group. The Linux Foundation's mission is to standardize Linux, support its growth, and promote its commercial adoption. It hosts and promotes the collaborative development of open source software projects. 

#### Why is Consortium marketing important?
Consortiums are marketing giants in the enterprise technology ecosystem. The success of the Cloud Native Computing Foundation today, the OpenStack foundation in the past, and many others, prove the value of technology thought leadership, branding, and engineering alignment. While these memberships sit withing the budget of the Community Team's Open Source program, the [Technical Evangelism team](https://about.gitlab.com/handbook/marketing/community-relations/technical-evangelism/) focuses on consortium marketing and integrating into the community to bring the GitLab technical perspective to the right conversations. 

#### Membership evaluation Criteria
Here are some of the factors we keep in mind when considering to join a new consortium:   

| Goals | Indicators | Examples of Indicators |
| ------ | ------ | ------ |
| Awareness Opportunities | Size of organization / contributor / member base: how many people are part of the organization’s community? | Monthly authenticated vs. non authenticated users visiting the sit. Annual users who perform a contribution activity of any kind. Annual users who perform a code-related contribution activity |
|  | Frequency and impact of marketing opportunities: what kind of communication channels do they have? Will we appear in official channels? How prominent is our placement?  | Channels may include: Social, Newsletters, Blogs, Events (Size of each event) |
| Ease of Collaboration | Dedicated marketing resources / point person: Does the organization have marketing capacity? | How mature is the organization's brand and marketing portions? Is marketing handled by volunteers, paid employees? |
|  | Relationship: how responsive is the person in charge of the relationship? | An alternative metric might be: Time-to-Execute for a few standard communication types. For example: from case study ideation to execution - 1 week? 1 month? 1 quarter? |
|  |  | Understanding of community gates: Can the foundation approve directly - is there a community feedback process, is board approval required, etc |
| Contribution and hiring pool opportunities | Active community: How active is the community and do they know their own community’s health, engagement, etc? | Frequency of contribution? Rate of adoption? | 
|  | Hiring opportunities: Are there opportunities to recruit from the community's talent pool?  | What is the growth of the community or foundation itself? Are there job opportunities within that software ecosystem (are people hiring contributors from this community in general)? Are there job boards, or other professional development activities? |
|  | What kind of informal and formal ways are there for us to contribute, and do they align with our interests? | Working groups? Advisory boards? Special initiatives? |
|  | Can GitLab participate in the project's roadmap in ways that creates mutual value?  | Example: Promoting adoption of GitLabCI to improve project testing and also expose contributors to GitLab's tools for their day-jobs/for-profit ventures.  |

#### Current memberships
We are currently members of the following consortiums:

| Consortium | Level |
| ------ | ------ |
| [Linux Foundation](https://www.linuxfoundation.org/) | [Silver](https://www.linuxfoundation.org/membership/members/) - board seat|
| [Cloud Native Computing Foundation (CNCF)](https://www.cncf.io/) | [Silver](https://www.cncf.io/about/members/) - board seat |
| [Fintech Open Source Foundation (FINOS)](https://www.finos.org/) | [Silver](https://www.finos.org/members) |
| [Continuous Delivery Foundation (CDF)](https://cd.foundation/) | [General](https://cd.foundation/members/) |

*Full details of our memberships with these organizations is available to GitLab team members only: [Community Relations Consortium Memberships](https://docs.google.com/spreadsheets/d/1h7DE5rDCj28ccYzJAZAnVwzEA1D0pxmrJoIqFLSlfL0/edit#gid=0).*

### Other sponsorship types
We have a small budget to sponsor events that allow us to engage with and build relationships among our current open source partners' communities. All other event sponsorship requests, are handled by our [field marketing team](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#3rd-party-events).  
