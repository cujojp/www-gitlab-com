---
layout: handbook-page-toc
title: "Strategic Marketing Metrics"
---


## Content Produced




## Web Traffic Analysis

[Website and handbook pages](https://datastudio.google.com/u/0/reporting/1jhpxOcfWp9B44smdc6Uv-tGMNyWoludX/page/JKsTB)

[Google Analytics for Beginners] (https://analytics.google.com/analytics/academy/course/6)


## Marketing Attribution Model

[Marketing attribution](https://app.periscopedata.com/app/gitlab/556414/Marketing-Linear-Attribution)

Marketing attribution - quick overview (https://www.bizible.com/blog/marketing-attribution-models-complete-list)


## Customer Reference Analytics

[Customer Reference Program](https://gitlab.my.salesforce.com/01Z4M000000slL1)


## Examples to consider

* [ChangeLog](https://about.gitlab.com/handbook/CHANGELOG.html)
* Surveys (Sales team)
* Analytics on issues managed - open/closed, internal vs. external
* Pathfactory



## CMO Challenge on how to measure

    * Website first (MRs?)
    * Metrics on views, etc.
    * Maybe update BOM colors/links at every stand up


## Useful links

[Tech Stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0)
