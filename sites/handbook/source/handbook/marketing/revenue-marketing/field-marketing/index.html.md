---
layout: handbook-page-toc
title: "Field Marketing"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Field Marketing during COVID-19 Pandemic
The health and safety of our Field Marketing team members continues to be a top priority.  Given the continuing situation with Covid-19, the Field Marketing leadership team have decided that we will not consider participating (sponsoring or hosting) in in-person events until August 1 at the earliest.  We will of course reassess as the situation changes but for now we appreciate your support and understanding on this decision. If you have any questions please feel free to reach out to the Sr. Director of Revenue Marketing, the Director of Field Marketing, or your direct line manager.  

If you are considering an in-person event in Q3 or Q4 there will be a vetting process, please open the field marketing issue, and tag your manager and the Global Director of Field Marketing. Starting in Q3, we will consider smaller in person events with no more than 40 people. 

Not to fear, the Field Marketing team is busying still helping to create pipeline via our other tactics. Check out all the awesome work on the [regional issue boards](/handbook/marketing/revenue-marketing/field-marketing/#whats-currently-scheduled-in-my-region). 

## In-Person Event Changes Due to COVID-19

### Owned Event Turned Virtual
   * FMM to notify MPM as soon as possible - this process takes considerable time to deconstruct existing setup and begin setting up as virtual
   * MPM to follow [this process](/handbook/marketing/events/#when-owned-offline-events-turn-to-virtual-owned-events)

### Field Event Turned Virtual
   * FMM to notify MPM as soon as possible - this process takes considerable time to deconstruct existing setup and begin setting up as virtual
   * MPM to follow [this process](/handbook/marketing/events/#when-offline-field-events--conferences-turn-to-virtual)

# Field Marketing

The role of field marketing is to support marketing messages at a regional level through in-person and virtual interactions (quality vs. quantity) coupled with multi-touch activities. Field Marketing programs are focused on relationship building with customers and prospects to support land and expand opportunities as well as pulling pipeline through the funnel more quickly.

# Field Marketing Goals

- Sales Acceleration
   - Engaging with existing customers
   - New growth opportunities
- Demand
   - Education
- Market Intelligence
   - Test out new messaging or positioning

# Account Based Marketing
Account based marketing is separate but sits next to field marketing. For info on account based marketing, please head over to the [ABM page](/handbook/marketing/revenue-marketing/account-based-marketing/).

# Types of Programs Field Marketing Runs

## GitLab Owned Field Events

### [GitLab Connect](https://www.youtube.com/watch?v=aKwpNKoI4uU)
GitLab Connect is a full or half day event with both customers and prospects in attendance sharing stories & lessons learned about GitLab. SAL's will be responsible for asking customers to speak and Marketing, through a combination of SDR outreach, database and ad geotargeting will drive attendance to the event. If you would like to propose a GitLab Connect in your city, please follow [these instructions](/handbook/marketing/events/#suggesting-an-event) for requesting an event. Interested in seeing a GitLab Connect in action? [Check it out.](https://www.youtube.com/watch?v=aKwpNKoI4uU)

### GitLab-Run Workshops
Field Marketers will work with Product Marketing & Technical Product Marketing to put together various types of workshops depending on the needs of the region. Workshops available to date can be found [here](https://gitlab.com/gitlab-workshops). 
Field Marketer's can also offer a Certificate of Completion to attendees post-event. See example [here](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2756).
## 3rd Party Events
We will sponsor regional 3rd party events in an effort to build the GitLab brand and also to gather leads. Different type of 3rd party events include, but are not limited to:

- DevOps Days
- Agile Events
- City run technology meetings
- Customer/prospect run DevOps events on invite 
- Executive relationship building events via companies like Apex Assembly & Argyle Executive Forum

### How to operationally set up an in-person event
Field Marketing works closely with our Marketing Program Managers to functionally set up and execute our in person events. For complete details on our shared process, including the epic/issue creation process, please review our [event execution page](https://about.gitlab.com/handbook/marketing/events/#event-execution). 

## Other Tactics

Being the marketing experts for the region, GitLab Field Marketers are also responsible for using other tactics to help generate leads to build pipeline. We have options such as survey tools, both of current employees and past employees based on the 3rd party vendor we use, webcasts, direct mailings, and also various digital tactics.

### Printfection Direct Mail Giveaways 

The below steps can be followed to set up and run a giveaway campaign using Printfection for field marketing. If you come across any steps that you can’t follow in Printfection, post in the [#swag Slack channel](https://app.slack.com/client/T02592416/C66R8N98F) so a Community Advocate can update your permissions levels.  

1. Register and send the items you will be using to Printfection by following [these steps](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#send-items-to-printfection-warehouse)

2. Bundle your items together by creating a [Giveaway Kit](https://help.printfection.com/hc/en-us/articles/360006335613-How-to-start-a-new-kit) in Printfection
    - The picture of the Giveaway Kit is what the customer will see. If you’d like a different image to show than what Printfection puts together as a preview, be sure to provide this to the support team
    - Select Standard packaging or choose to create a custom box (a new custom box will incur additional expenses). If you’d like to use already created custom packaging (such as the GitLab branded Poly Mailers), check with the advocates in the #swag slack channel to make sure it’s okay for you to use 

3. Set up a [Giveaway Campaign](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#create-a-new-giveaway-campaign) 
    - [Add the kit](https://help.printfection.com/hc/en-us/articles/360026589734-Using-kits-in-Giveaway-campaigns) you created as an item in the Giveaway Campaign 
    - The name of the giveaway campaign will be visible to the customer
    - Change from Paused to Running when you’re ready to run the campaign 

4. Add a Printfection code column to your event spreadsheet with your leads. Copy over unique codes (generated above in the Printfection Giveaway Campaign) to assign a code to each lead

5. Work with your MPM to set up the Printfection URL & unique codes in your email copy: 
    - Place the Giveaway Campaign URL in your email copy (found on the Overview tab of the campaign in Printfection) 
    - A custom Marketo field will be used to append the unique Printfection codes to the URL for each lead (example of how it works [here](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1195#note_325404319)) and to place the code under the URL 

6. Complete list upload prior to the email send date (example list upload [here](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/2343))

7. Work with your MPM to determine post redemption email needs. The options are follow up email and an automated confirmation email. The confirmation email triggers upon redemption of codes, the follow up email is sent at one time to a group of receipients. If an automated cofirmation email is needed, define what status in SFDC should trigger the follow up email
using the Direct Mail campaign status progressions. (for example, when a person's campaign status changes to `Queued` it could trigger a confirmation email and when the status changes to `Shipped` the SDRs will begin follow up sequence (example [here](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1195#follow-up-planned-from-field-marketing-fmc))

8. Once all above steps are complete, the Giveaway Campaign is ready to run. Make sure the Giveaway is changed from Paused to Running if you did not do this already in Step 3

9. When the campaign is running, monitor Redemptions, Shipping, and Costs under the Giveaway Campaign Overview in Printfection 
    - Update redemptions/shipments in event spreadsheet if needed to notify SDRs (add additional columns to track)
    - Update SFDC status for leads once they redeem a code or item is shipped, depending on what was decided in Step 7 

10. When a campaign is over and you no longer need the items in Printfection, follow [these steps](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#removing-products-from-printfection) to remove items from Printfection. If inventory is not yet 0, you will need to ship the items back to yourself/desired recipient before archiving. Until this is done, $25/month will still be charged per item. The MPM must be instructed as to when the printfection codes are no longer needed on person records. This will signal that the MPM can now run a batch smart campaign to remove the printfection codes from the person records, thus freeing up the custom field for use in the future.

**Fees Involved:** 
- Cost of the items you are using in the giveaway
- $25/month per item stocked in Printfection (as noted in Step 12 [here](https://about.gitlab.com/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#creating-the-send-order)
- Fulfillment and shipping fees for each order placed once the giveaway is running (there is a Cost Estimate under campaign settings but please note actuals have ended up higher than their estimate so far) These will be on a Printfection invoice, more details to come on the invoice process. 


### EMEA Giveaway/SWAG Campaigns

* When printfection is not an option, EMEA Field Marketing utilise the services of vendor [Ten&One](https://www.tenandone.com/) to source, fullfill and ship giveaways/swag [EMEA SWAG](https://about.gitlab.com/handbook/marketing/events/#emea-field-marketing-swag). Please create an issue utilizing the [EMEA Swag Request Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/blob/master/.gitlab/issue_templates/EMEA_SWAG_Request_template.md) or contact the EMEA FMC for further information and options.  


## Digital Tactics 

* To run plays where we are targeting a specific geography or where we would like to propose content syndication, we work through our Digital Marketing Programs team. Please create an issue utilizing DMPs [Paid Digital Request Issue Template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=paid-digital-request) if you'd like them to do work for you. 

* To run plays where we are targeting a specific list of accounts, we work with our ABM team utilizing [this issue template](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/blob/master/.gitlab/issue_templates/Demandbase_Campaign_Request_Template.md). 

## Account-Centric Micro-Campaign

The purpose of the regional integrated micro-campaign is to build and drive a specific strategy in a region (with a specific list of targeted accounts/look-a-like accounts). This could be based on intent data pulled from our ABM tool, DemandBase, or it could be based on accounts you’ve agreed to target with your SAL. A micro-campaign must include a minimum of 2 different account-centric tactics.

**For example:**
* If you are planning a virtual Lunch & Learn and want to utilize LinkedIn InMail as well, those 2 account-centric tactics together make up a micro-campaign. However, if you are planning a Lunch & Learn on its own without any additional tactics, that is just considered an [Individual Tactic](/handbook/marketing/revenue-marketing/field-marketing/#individual-tactics).

### What does Account-Centric mean? 
At times, Field Marketing will run an account-centric approach to its in-region campaigns. Account-Centric is an approach where both sales and marketing are focusing their efforts on a very specific list of targeted accounts in order to increase engagement and ultimately pipeline creation within this list of accounts. The SAL/FMM/SDR are responsible for outlining which accounts are account-centric and accounts can easily pop in and out of being in the account-centric play based on the strategy that’s being executed. 
	i.e. If a FMM is targeting to get 15 people into an interactive roundtable discussion, that will greatly impact the number of accounts that are targeted for that specific tactic vs if they were trying to 100+ people into that same discussion. 

### How to track the ROI of your Account-Centric approach 

In order to track engagement, you need to start with a baseline and over time, track the progress on how engaged that account is with us. Right now this is a manual process where this [SFDC report](https://gitlab.my.salesforce.com/00O4M000004e4Ll) should be modified by pulling in all the accounts that are in the account-centric GTM Strategy per rep and then you must take a snap shot in time (SFDC does not allow for historical data pulls) by exporting the data and saving to a gsheet. [Example of gsheet](https://docs.google.com/spreadsheets/d/1m7xxoKcXW3Lq8eXIPg5KIOMGXIItp7MEZeBXAFfszOI/edit#gid=206509375&range=A1). 

Account-Centric is different from Account Based Marketing in the sense that ABM focuses on accounts as markets of one versus account-centric targeting a group of accounts.  At GitLab we run a 3-Tiered approach to ABM focused around our ICP and target 100-150 accounts in our ABM strategy at any given time.  For complete details on how GitLab runs its Account Based Marketing, please head to the [GitLab ABM page](/handbook/marketing/revenue-marketing/account-based-marketing/#what-is-account-based-marketing). 

At GitLab, we utilize Bizible for attribution tracking. In order to track the touchpoints of your campaign, modify [this linear IACV SFDC report ](https://gitlab.my.salesforce.com/00O4M000004aDSN). 
* If your campaign has a SFDC campaign with leads associated to it, then you will pull in the SFDC Field `Salesforce Campaign` equals <enter in the name of your SFDC Campaign>. 
* If your campaign does NOT have a SFDC Campaign with leads associated OR you also are wanting to track the opportunities from your digital spend, then you would add in the SFDC `Ad Campaign Name` equals <enter in the UTM from your campaign>. We use the same UTM per channel, so all channels (paid ads, LinkedIn Mail, DemandBase, etc.) will pull all influenced iacv. Stay tuned for more details on Bizible. 

### Account-Centric Micro-Campaign Team 
* Field Marketing Manager - overarching DRI responsible for opening [the micro-campaign issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics), reviewing the intent data from the ABM DRI, and building the strategy based on what the intent data is suggesting (i.e. are a lot of folks searching to Kubernetes? Is it digital transformation? What call to action do we have? Do you want a direct mail piece involved? Do you want to drive to an On-Demand or scheduled webcast?), owning, and driving the mico-campaign, including epic and issue creation and communicating timelines and actions needed of other team members with relevant SLAs from request to delivery. 

* Account Based Marketing (ABM) Manager - will work with FMM to produce target account list based on intent data and known targets & will also lead strategy session with Demandbase Campaign Manager, should the FMM decided its part of the strategy to deploy ads via Demandbase. 

* Sales Development Rep (SDR) - Will assist in building out the actual target people & build Outreach cadence to support agreed upon CTA. 

* Digital Marketing Programs - Can assist in deploying paid digital strategies (ex: display ads, paid search, paid social, and/or paid publisher placement) and geo focused ads via our digital agency. They can also help get our digital agency involved should the FMM want to engage with the agency. 

* Marketing Program Managers - Setup of Marketo and Salesforce campaigns with correct program type to support the tactic. Guidance on system tracking, setup of Marketo email follow up, and addition of needs to existing email nurture programs (all as needed). Facilitate new program types with MOps and other MPMs as necessary.

### Steps for the Creation and Organization of Micro-Campaign Epics & Issues  

#### 1. FMM opens overarching issue

*FMM opens a field marketing issue using the [Micro-Campaign and Individual Tactics Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics)*, listing the tactics to be used from the list below in step 2. At this point, the issue is in `PLAN` stage.

**WHY? The purpose of opening the overarching micro-campaign issue is to discuss the plan with any teams that would require work and gain agreement on SLAs before opening epics and issues.** *Remember - as of today, epics cannot be made confidential, they cannot show up on issue boards, and there is no such thing as an epic template.* 

**Here is what needs to be included before moving from `PLAN` to `WIP`:**  
   - Agreement on SLAs and scope between other teams involved in selected tactics  - [SLA workback doc](https://docs.google.com/spreadsheets/d/1YXriQ1clvYyBn-TDbbCVvNP6NEbrAF-0w6tIHKhDeZM/edit#gid=1983708280)
   - Formulation of a clear strategy and understanding of customer journey  
   - Completion of all fields in the `Details` section of the micro-campaign template

*For anything that’s listed in the `Tactics included` section of the template the FMM needs to be clear about their specific ask & what the desired outcome will be. If the FMM does not know, it's ok to set up a call to talk through ideas and strategies.*  

#### 2. FMM opens specific tactic issues

*FMM opens additional issues relevant to their micro-campaign, if required.*

**WHY? The purpose of opening the issue is so that each individual tactic shows up on the FMM boards.** *Remember - as of today, Epics cannot be made confidential, they cannot show up on issue boards, and there is no such thing as an epic template.* 

**Tactic options and related issue templates (and handbook pages also linked for details):**
* **Self-Service Virtual Event With or Without Promotion**: ([details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#self-service)) FMM to open an issue utilizing the [Self-Service Event Issue Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=self_service_event) 
* **Sponsored Webcast** (3rd party hosted): ([details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#sponsored-virtual-events)) FMM to open an issue utilizing their region's [Field Event Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/tree/master/.gitlab/issue_templates)
* **Webcast** (GitLab Webcast License, must align to [virtual event decision tree](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#gitlab-virtual-events-decision-tree)): ([details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#webcast)) FMM to open an issue utilizing their region's [Field Event Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/tree/master/.gitlab/issue_templates) and FMM to also follow process outlined [here](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/) to submit the Webcast Request Issue
* **Sponsored Virtual Conference** (3rd party-hosted virtual conference, with a booth): ([details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#sponsored-virtual-events)) FMM to open an issue utilizing their region's [Field Event Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/tree/master/.gitlab/issue_templates)
* **Direct Mail**: ([details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/direct-mail/)) FMM to include details in the [Micro-Campaign and Individual Tactics Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics)
* **Survey**: ([details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/surveys/)) FMM to include details in the [Micro-Campaign and Individual Tactics Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics)  
* **Digital Marketing Program Tactics:** ([details](/handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#digital-campaign-types)) FMM to include details in the [Micro-Campaign and Individual Tactics Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics)

#### 3. FMM creates tactic epics

*FMM follows the instructions for epic creation based on the tactics decided.*

**WHY? The purpose of opening the epics and issues is to organize the campaign in GitLab with the epic being equivalent to the project/campaign and issues being equivalent to the tactical execution steps to complete the campaign.**

**How to create the epic:**
* Where to create the epic: [Marketing Epic Repo](https://gitlab.com/groups/gitlab-com/marketing/-/epics?sort=created_desc)
* How to create the epic: Click "New Epic" at the top right of the screen
* What to name the epic: The naming convention is listed in the epic code for each specific tactic.
* What to choose as the due date: this should be the same as your micro-campaign issue due date. For example - If your micro-campaign involves 3 different tactics and the last tactic is set to close out on December 5, 2020, that should be your due date.
* How to link the epic to the issue: In your epic click on Epics + Issues - Add - Add an existing issue - Link your issue

**Micro-Campaign Epic Code**  
This epic will include the general details of your micro-campaign.  

```
<--- Name this epic using the following format, then delete this line: [Micro-Campaign Tactic Name] - [3-letter Month] [Date], [Year] --->

## [Micro-Campaign Issue >>]()

## :notepad_spiral: Details 
(copy and pasted from your micro-campaign issue)  

/label ~"mktg-status::wip" ~"Field Marketing" ~"FMM-MicroCampaign" ~"FMM-Other Tactics"

```
**Links to epic code for FMM to use to create additional epics (based on tactic):**  
These epics will fall under your micro-campaign epic.  
* [Self-Service Virtual Event With or Without Promotion](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#self-service-virtual-events-with-or-without-promotion) - Please note there are two different epic codes listed, dependent on whether you are utilizing promotion or not
* [Sponsored Webcast](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-sponsored-webcast)
* [Sponsored Virtual Conference](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-sponsored-virtual-conference)
* [Webcast](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/) - MPM will open this if webcast request was accepted
* [Direct Mail](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/direct-mail/)
* [Survey](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/surveys/)
* Digital Marketing Program Tactics - Of the DMP tactics, the only tactic that requires its own epic creation is Content Syndication.
   * [Content Syndication](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication)

#### 4. FMM creates relevant issues and assigns DRIs and due dates

*At this point the FMM has the main issue (for agreement by teams), the tactic issue(s) (to show up on boards), and the tactic epics (to capture overarching tactic plan and organize issues). Now FMMs will create the issues to request the work and timeline of teams involved in each tactic.*

In each epic code (from step 3), there are issue template links included that indicate what issues need to be created (example - follow up email, facilitate tracking, etc.) - if they do not say "optional" then they are required. The FMM will create each issue and link to the epic so that all tactical issues are organized together clearly in GitLab.

The FMM will designate DRI as indicated and set due dates based on [SLAs](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/#timeline-guidelines) for each item that is being requested. [SLA workback sheet](https://docs.google.com/spreadsheets/d/1YXriQ1clvYyBn-TDbbCVvNP6NEbrAF-0w6tIHKhDeZM/edit#gid=1983708280) is available to help you. 

**Note: This is essentially creating the work-back execution plan using issues, DRIs, and due dates.**  
If the date of the tactic is changed, the FMM is responsible for updating the due dates of all issues accordingly. It is also important to remember that the DRIs on the issues are not notified when due dates are added. If you create an issue and then add due dates at a later time (or change due dates), please remember to ping the DRIs in the issue so they are aware dates have been added or changed.

#### 5. Organizing Epics
Your micro-campaign issue and epic is an overview of your micro-campaign strategy and each individual tactic supporting the micro-campaign requires its own issue and epic. When you add individual tactic epics to the micro-campaign epic, the micro-campaign epic automatically becomes the [Parent Epic](https://docs.gitlab.com/ee/user/group/epics/#relationships-between-epics-and-issues). Each individual tactic epic, when nested under the micro-campaign epic, automatically becomes the [Child Epic](https://docs.gitlab.com/ee/user/group/epics/#relationships-between-epics-and-issues). Whichever epic you nest the other epics under will automatically become the Parent Epic, so remember to always add your individual tactic epics to the micro-campaign epic and not the other way around.

For example - You have created a micro-campaign that will include a self-service event with promotion, a survey, LinkedIn InMail, and DemandBase targeted ads. 
* You will create an epic for your micro-campaign per the [above instructions](/handbook/marketing/revenue-marketing/field-marketing/#3-fmm-creates-tactic-epics).
* Your self-service event with promotion will require a separate issue with its own epic. 
* Your survey will also require a separate issue with its own epic. 
* Your LinkedIn InMail will require its own issue (the only DMP tactic that currently requires an epic is Content Syndication).
* Your DemandBase ads will require its own issue (ABM tactics do not currently require epics).

**To organize your various tactic epics under your micro-campaign epic:**  
* Click into your micro-campaign epic
* Scroll down to Epics & Issues
* Click Add an Epic
* Paste the epics from your other tactics into this section and GitLab will automatically organize the tactic epics under your micro-campaign epic

## Individual Tactics

Remember, an account-centric micro-campaign consists of at least 2 different account-centric tactics. If you are planning an individual tactic (account-centric or not), follow the below steps. Refer to the information provided in [Steps for the Creation and Organization of Micro-Campaign Epics & Issues](/handbook/marketing/revenue-marketing/field-marketing/#steps-for-the-creation-and-organization-of-micro-campaign-epics--issues), for _how_ to create the issues & epics if needed.  

1) Open tactic issue (see [here](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#2-fmm-opens-specific-tactic-issues) for issue links)  
2) Open tactic epic (see [here](https://about.gitlab.com/handbook/marketing/revenue-marketing/field-marketing/#3-fmm-creates-tactic-epics) for epic code)    
3) Create relevant issues (from links in the epic), assign DRIs and due dates  

Reminder for what what needs to be included before moving from PLAN to WIP:  
- Agreement on SLAs and scope between other teams involved in selected tactics - [SLA workback doc](https://docs.google.com/spreadsheets/d/1YXriQ1clvYyBn-TDbbCVvNP6NEbrAF-0w6tIHKhDeZM/edit#gid=1983708280)
- Completion of all fields in the Details section of the template

### Examples of Individual Tactics

#### Content Syndication

You decide you would like to create a Content Syndication piece. Here is how you would utilize the above [Steps for the Creation and Organization of Micro-Campaign Epics & Issues](/handbook/marketing/revenue-marketing/field-marketing/#steps-for-the-creation-and-organization-of-micro-campaign-epics--issues) to do so:

1. Content Syndication is a DMP tactic and per the [instructions for opening a tactic issue](/handbook/marketing/revenue-marketing/field-marketing/#2-fmm-opens-specific-tactic-issues) you would follow the Digital Marketing Program Tactics instructions and open a [Micro-Campaign and Individual Tactics Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics).
1. Since Content Syndication is the only DMP tactic that requires an epic, you would then follow the [epic creation instructions](/handbook/marketing/revenue-marketing/field-marketing/#3-fmm-creates-tactic-epics) and create the [Content Syndication](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication/) epic.
1. Finally, you would follow the instructions for [creating relevant issues and assigning DRIs and due dates](/handbook/marketing/revenue-marketing/field-marketing/#4-fmm-creates-relevant-issues-and-assigns-dris-and-due-dates).

#### Self-Service Virtual Event With or Without Promotion

You decide you would like to host a Self-Service Virtual Event. Here is how you would utilize the above [Steps for the Creation and Organization of Micro-Campaign Epics & Issues](/handbook/marketing/revenue-marketing/field-marketing/#steps-for-the-creation-and-organization-of-micro-campaign-epics--issues) to do so:

1. Per the [instructions for opening a tactic issue](/handbook/marketing/revenue-marketing/field-marketing/#2-fmm-opens-specific-tactic-issues) you would follow the Self-Service Virtual Event instructions and open a [Self-Service Virtual Event Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=self_service_event).
1. You would then follow the [epic creation instructions](/handbook/marketing/revenue-marketing/field-marketing/#3-fmm-creates-tactic-epics) and create the [Self-Service Virtual Event Epic](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#self-service-virtual-events-with-or-without-promotion) (please note there are two different epic codes listed, dependent on whether you are utilizing promotion or not).
1. You would then would follow the instructions for [creating relevant issues and assigning DRIs and due dates](/handbook/marketing/revenue-marketing/field-marketing/#4-fmm-creates-relevant-issues-and-assigns-dris-and-due-dates).
1. Finally, you would also continue through the additional steps for setting up your Self-Service Virtual Event that are listed following the Self-Service Virtual Event epic instructions [here](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#best-practices).

#### Micro-Campaign Training Video

[Video training on how to set up a Micro-Campaign](https://drive.google.com/drive/folders/1nTQOO5jszlwIivy_bCjqA0a6RXnKwQtt). This video walks you though how to set up a Self Service webcast with promotion and LinkedIn InMail. Two tactics = Micro Campaign. 

#### Example Micro-Campaign for Training

*   [Micro-Campaign Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1072)
    *   [Micro-Campaign FM Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1396)
        *   [Self-Service Virtual Event with promotion Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1073)
            *   [Self-Service Virtual Event  with Promotion FM Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1397)
            *   [Add to Nurture Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2960)
            *   [Follow Up Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2959)
            *   [Invites and Reminders Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2958)
            *   [List Clean and Upload Issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/2712)
            *   [Facilitate Tracking Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2957)
        *   [Paid Survey Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1074) (No separate FM issue since information is in overall Micro-Campaign Issue)
            *   [Gated Content Request Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2965)
            *   [Add to Nurture Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2963)
            *   [Follow Up Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2962)
            *   [Facilitate Tracking Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2961)
        *   [LinkedIn Inmail Issue](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/issues/136) (No epic required)
        *   [DemandBase Targeted Ads Issue](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/issues/136) (No epic required)

## FAQ's -  Micro Campaigns and Other Tactics
1. If I am only running 1 tactic, do I need to create the micro campaign issue? Answer: No, you do not. A microcampaign includes at least [2 account centric tactics](/handbook/marketing/revenue-marketing/field-marketing/#account-centric-micro-campaign).
1. If I want to run ads through our ABM vendor do I need to create a micro campaign issue? Answer: No, you do not. A microcampaign includes at least [2 account centric tactics](/handbook/marketing/revenue-marketing/field-marketing/#account-centric-micro-campaign). Ads with our ABM vendor is only 1 tactic. 

## Reminders -  Micro Campaigns and Other Tactics
1. **Labels** - be aware of your labels. Please be mindful that you're adding the correct labels. 
1. Epic label issue - There is a glitch right now which may keep the labels from auto-assigning. In order to have the labels added to your epic, you'll need to copy the /label line from the epic code into a comment to pull over all labels. [Quick video how-to](https://drive.google.com/open?id=1gK8G4CBaqcgWgB6b2A1BRSQCma5GfJyt). 2 mins
1. If you create an epic & sub issues and DO NOT have a date, then when you go back and add dates then you MUST ping the assigners - could be FMC, MPM, OPS, etc.
1. When you go from plan to WIP that's when everything starts - from the SLA workback as well. 
1. The details section is so important -  what that tells the MPM/FMC how this program should flow. You can't move your issue to the WIP stage until the details are added  - This is where if attention to detail isn’t your BFF, you need to make it your BFF. ;) 

# AMER Field Marketing Vendors and Tools 
We sometimes work with third party vendors and use other tools for outreach, event production etc. Below is a list of whom we work with currently and the epic that tracks whom we have evaluated/worked with in the past.
* Emissary.io - in an effort to help sales gain account intelligence 
* Banzai - to supplement event recruiting 
* [Thnks](https://www.thnks.com/how-it-works/) - platform to send gifts to recipients with the option to accept gift or donate to charity 
    - There is a service fee on each order which sits around 16% (for example, a $50 giftcard will cost us $60 to send via Thnks)
    - `@lilphil` currently has an admin login and can add others as users, reach out to be added or for more info on using 
    - Example Thnks [here](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1380#package-swag-and-event-assets)
* Printfection - used for direct mail giveaways, see more details [here](/handbook/marketing/revenue-marketing/field-marketing/#printfection-direct-mail-giveaways) 
* [FM vendor evaluation epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/441)

# AMER Field Marketing Event Venue Tracking
The below epic is for tracking venues we would like to utilize for future events, or as a way to evaluate event venues we have already worked with to note the pros and cons of various event space across different regions.
*  [AMER Field Marketing Event Venues Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/625)

# Corporate Memberships Owned by GitLab Field Marketing 

* [AFCEA](https://www.afcea.org/site/) - Membership is handled by the Public Sector Field Marketing Manager. Account information is stored in the marketing 1Pass vault. 
* [ACT-ICT](https://www.actiac.org) - Membership is handled by the Public Sector Field Marketing Manager. Please ping the PubSec FMM for details if you'd like to join. 
* [Charleston DCA](https://www.charlestondca.org/) - Membership is handled by the Public Sector Field Marketing Manager. Please ping the PubSec FMM for details if you'd like to join.
* [G2xExchange](https://www.g2xchange.com/join-today/) - Membership is handled by the Public Sector Field Marketing Manager. Please ping the PubSec FMM for details if you'd like to join.
 
# What's currently scheduled in my region?

| Region | FM DRI | GitLab User ID |
| :----- | :----- | :------------- |
| [AMER East NE & SE](https://gitlab.com/groups/gitlab-com/marketing/-/boards/915674?&label_name[]=East) | Ginny Reib | `@GReib` |
| [AMER East-Central](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1105137?&label_name[]=East%20-%20Central) | Jake Sorensen | `@JSorensen` |
| [AMER West-PacNorWest](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933457?&label_name[]=West) | Rich Hancock | `@rhancock` |
| [AMER West - NorCal/SoCal/Rockies](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933457?&label_name[]=West) | Rachel Hill | `@rachel_hill` |
| [AMER Public Sector](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933456?&label_name[]=Public%20Sector) | Helen Ortel | `@Hortel` | 
| [APAC](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933488?&label_name[]=APAC) | Pete Huynh | `@Phuynh` |
| [EMEA Southern Europe](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1426531?&label_name[]=Southern%20Europe) | Tina Morwani | `@tmorwani` | 
 [EMEA MEA](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1426540?&label_name[]=MEA) | Tina Morwani | `@tmorwani` |
| [EMEA Northern Europe](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438252?&label_name[]=Northern%20Europe&label_name[]=Russia) | Kristine Setschin | `@ksetschin` |
| [EMEA UK/I](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438265?&label_name[]=UK%2FI) | Kristine Setschin | `@ksetschin` |
| [EMEA Central Europe](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438243?&label_name[]=Central%20Europe&label_name[]=EMEA&label_name[]=Europe%20CEE) | Jeffrey Smits | `@JDSmits` |
| [EMEA CEE](hhttps://gitlab.com/groups/gitlab-com/marketing/-/boards/1440436?&label_name[]=EMEA&label_name[]=Europe%20CEE) | Jeffrey Smits | `@JDSmits` |
| [EMEA Russia](hhttps://gitlab.com/groups/gitlab-com/marketing/-/boards/1484015?&label_name[]=Russia) | Kristine Setschin | `@ksetschin` |

**NOTE:** to see the full list of events, you need to be logged into your GitLab account. There are times we make issues private. 

To find out what upcoming events GitLab will be involved in please visit our [Events Page](/events/).  

For details on how we handle events please visit the [GitLab Event Information](/handbook/marketing/events/) page.

# Suggesting an Event or Tactic
If you are interested in requesting Field Marketing's support for a particular in-person or virtual event or tactic, please follow the process listed on our [Events Page](/handbook/marketing/events/#suggesting-an-event).

# Field Marketing planning 
It is our goal to have planning for each qtr completed four (4) weeks before the next quarter starts. At this time, we have the following time line set for regional plans: 
* Q3 plan is due July 1 
* Q4 plan is due Oct 1 
* Q1 plan is due Jan 6

A standard deck will also be used to ensure consistency across the world in terms of how the plan is presented. The SSOT of the plan remains in GitLab on our [various issue boards](/handbook/marketing/revenue-marketing/field-marketing/#whats-currently-scheduled-in-my-region). 

* [FY21 Standard deck](https://docs.google.com/presentation/d/1mLaL7YeCDD8gD4r7NAbP6ui1_iDghZptpO-AOpQpV_E/edit#slide=id.g29a70c6c35_0_68). 
   * Note: The deck is additive. You are only responsible for completing the slides that are due for the quarter we are in. 
   * As the slides are due one (1) month before the end of the quarter, if you have a campaign/tactic that you want to highlight on the recap slide that has not been completed, please share this with your manager and propose a date when you will have that slide completed. 

# Field Marketing + Channel Marketing 

In April 2020, as part of our GTM strategy, GitLab launched its [channel sales program](https://about.gitlab.com/handbook/resellers/). As part of that program, each Channel Account Manager (CAM) will have a maximum of [3-5 Select Channel partners](https://docs.google.com/spreadsheets/d/1-EE7vChGkDeyJxoM-LjVmUdwYwboxBmq8_42hjHGw_w/edit#gid=241847859&range=A2) for [P0 and P1 regions](https://gitlab.com/groups/gitlab-com/-/epics/249).  After the CAM has the 3-5 Select Channel partners identified (note: FMM could work with Open partners depending on their MDF proposal, and that will be handled on a case by case basis), it's the responsibility of the regional FMM to work with the channel team in the following manner: 

1. Include Select Channel partners as a portion of their overall territory plan with the focus being lead generation. 
2. Regional Channel Marketing and Enablement 
    1. In FY21 - Channel marketing will be the DRI for this and is aligned with the [Channel Partner Enablement team](https://about.gitlab.com/job-families/sales/program-manager-field-enablement/). It's the responsibility of FMM to share feedback they receive in terms of how enabled the channel is. 
    
3. Lead Management in the Channel: 

    _This is a WIP in process. As we start to work with channel partners, we will refine this process. _

    1. GitLab will own the leads and lead management if the Channel Partner is participating in a GitLab led initiative. 
    2. The Channel Partner will own leads and lead management if GitLab is participating in a Channel Partner lead initiative.
    
4. Regional MDF management
    3. As outlined in the [Channel Handbook - MDF section](https://about.gitlab.com/handbook/resellers/#the-market-development-funds-mdf-program), GitLab Open and Select partners have access to the proposal-based GitLab Marketing Development Funds (MDF) Program. 
        1. In FY21 - DRI for approvals, budget tracking, ROI measurement support will be the Channel Marketing team with Field being involved in the sign off/approval of plan to ensure the region can support the execution of the MDF plan in region. 

## For more details on what the Channel Programs team does, please head to [their page](https://about.gitlab.com/handbook/sales/channel/). 

## Here’s a quick rundown (not exhaustive) of what the Channel Program team handles: 

1. Manage/create SPIFFs 
2. Manage Partner Advisory Boards
3. Set up MDF program
4. Manage the updates of the handbook 
5. Operationally Manages Channel Partner Portal

## Channel Marketing Handles

1. Updates content in the Channel Partner Portal - content, programs, campaigns, creative
2. Updates the [Sales enablement resources](https://about.gitlab.com/handbook/marketing/product-marketing/sales-resources/) - partner deck
    1. Partner version of the customer facing deck
    2. 4 slides about why Partners should work for us (SME for those slides would be Programs team)
3. National / worldwide partner events - Partner Summit (aligned with SKO), roadshows, etc.
4. Campaign Design for Broad Channel Program 
    1. Demand Campaigns: generally crafted from existing GitLab campaigns 
    2. Partner Recruit Campaigns
    3. Digital Amplification (Digital & Social)
    4. Distribution Marketing
        1. Support partner recruitment programs
        2. Support scalable demand generation programs
        3. Support scalable enablement programs
    5. National Select partner campaigns & MDF management 
    6. National Select partner event management
    7. Executing & Promoting SPIFFs 
    8. Joint co-branded GTM solution briefs w/ Select Partners
    
# PubSec Team Lead - Staff Field Marketing Manager
Within our public sector team we have a team lead. Our PubSec Team Lead will:

* Lead PubSec Field marketing strategy and direction 
* Responsible for the defense GTM strategy, tactics, execution
* Supports channel - Alliance, SI’s DoD
* Leads content collaboration efforts from Field Marketing with Strategic Marketing
* Represents Field Marketing team on the public sector team call, speaking and documenting content to be discussed
* Leads GitLab Connect or major event in the DC area when pulling in multiple public sector verticals
* Leads the bi-weekly call with Director of Public Sector
* Leads common work process issues as they arise - i.e. lead routing
* Manages budget for overarching PubSec campaigns 

# GitLab Company Information (Including Tax ID)
[Useful Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) 

# AMER Field Marketing iPad Purchasing and Setup Instructions
 
## Purchasing Details  
 
[iPad Pro 12.9 inch/256GB/wifi](https://www.apple.com/shop/buy-ipad/ipad-pro/12.9-inch-display-256gb-space-gray-wifi)  
[iPad Pro 12.9 inch Smart Keyboard Folio](https://www.apple.com/shop/product/MU8H2LL/A/smart-keyboard-folio-for-129-inch-ipad-pro-3rd-generation-us-english)    
* When purchasing, please utilize the GitLab Business Account for corporate discounts. The Apple store/online representative will look up the GitLab Business Account associated with GitLab's 268 Bush St., San Francisco, CA  94104 address.
* Do not purchase AppleCare
* iPads can only be purchased by the regional Field Marketing Manager

## iPad Tracking 
To ensure we know who within the company currently has a company owned ipad, [please see here](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/1031). 
## Setting Up Logins
 
**iPad Password**  
* Utilize the password listed in the notes section of the AMER Field Marketing Apple ID located in the Marketing 1Pass
 
**Field Marketing Apple ID**  
* Log in to the AMER Field Marketing Apple account utilizing the username and password listed under the AMER Field Marketing Apple ID located in the Marketing 1Pass. Please take note of the additional information listed in the notes section regarding cell phone verification.

**Marketo Check-in App**  
* Download the Marketo app from the App Store. Sign in using your Marketo login and follow the instructions [HERE](/handbook/marketing/events/#marketo-check-in-app) for details on using the app during events.

**Google Drive and Slides**  
* Follow the instructions [HERE](/handbook/marketing/product-marketing/demo/conference-booth-setup/#ipads) to set up Google Drive and Slides

# Working with Field Marketing

## Requests to Field Marketing leadership team

If you are a Field Marketing Manager and you would like to make a request in order to achieve better business results aligned to our values, please submit an issue using the [Field Marketing Leadership request issue template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=leadership_request).

FM leadership team will review all requests on a bi-weekly basis, and will prioritize as necessary or will share with you relevant issues if the request is already being worked on. 

**Why we use this template:**  
Funneling similar requests from a global team and addressing shared pain points in a single issue with a [DRI](https://about.gitlab.com/handbook/people-group/directly-responsible-individuals/) will help to reduce noise and churn for other teams we collaborate with and ideally helps to improve a process that works for the WW Field organisation. 

This issue should not be used as a shortcut to get questions answered but for request where FM leadership can help to improve a process which does contribute to our success as a team.

## Field Marketing Labels in GitLab
The Field Marketing team works from issues and issue boards. If you need our assistance with any project, please open an issue and use the `Field Marketing` label anywhere within the GitLab repo.

**General Field Marketing Labels:**  
* `Field Marketing`: Issue initially created, used in templates, the starting point for any issue that involves Field Marketing  
* `FY21-Q1`, `FY21-Q2`, `FY21-Q3`, `FY21-Q4`: What event or activity is set to take place or be due in this quarter 
* `mktg-status::plan`: work that is proposed, in an exploratory state
* `mktg-status::wip`: work in progress that has been accepted and assigned to a DRI
* `Events` : Issues realted to events - this label is held at the gitlab.com level 
* `FMM-Other Tactics`: Issues related to non-event tactics
* `FMM-MicroCampaigns`: Issues related to regional integrated micro-campaign, which are built to drive a specific strategy in a region

**Regional Field Marketing Labels:**

* `APAC`: Issues that are related to the APAC sales team  
* `EMEA`: Issues that are related to the EMEA sales team  
* `Central Europe`: Issues that are related to the EMEA Central Europe (DACH) sales team  
* `Europe CEE`: Issues that are related to the EMEA Central Eastern Europe sales team  
* `Southern Europe`: Issues that are related to the EMEA Southern Europe (France, Italy, Spain, Portugal, Greece, Cyprus) sales team  
* `Northern Europe`: Issues that are related to the EMEA Northern Europe (Nordics & Benelux) sales team  
* `UK/I`: Issues that are related to the EMEA UK & Ireland sales team  
* `Russia`: Issues that are related to the EMEA Russia & Ukraine sales team  
* `MEA`: Issues that are related to the EMEA MEA (Middle East & Africa) sales team   
* `East`: Issues that are related to the East sales team   
* `East - Central`: Issues that are related to the US East-Central sales team  
* `East - NE`: Issues that are related to the US East- NE sales team   
* `East - SE`: Issues that are related to the East-SE sales team    
* `WEST`: Issues that are related to the US West sales team  
* `WEST - Bay Area`: Related to the US WEST Bay Area sales team  
* `WEST - BigSky`: Issues that are related to the US WEST Midwest sales team  
* `WEST - FM Planning`: Issues related to West FM planning  
* `WEST - FMM`: Issues related to the West FMM  
* `WEST - PacNorWest`: Issues that are related to the US WEST Pacific North West sales team  
* `WEST - SW`: Issues that are related to the US WEST Southwest and SoCal sales team  
* `Public Sector US`: Any issues related to US Public Sector  
* `FMM AMER PubSec`: All issues and tasks related to the FMM AMER for PubSec  
* `AMER - DOD`: AMER DOD tactics  
* `AMER - CIV`: AMER Civilian tactics  
* `AMER - SLED`: AMER SLED tactics   
* `AMER - NSP`: AMER National Security tactics  
* `FMC AMER NE/SE/PubSec`: Issues related to the FMC AMER East & PubSec      
* `FMC AMER NE/SE/PubSec - Tracking`: Deadline tracking related to FMC AMER East & PubSec   
* `FMC AMER NE/SE/PubSec - Swag`: FMC AMER East & PubSec tracking for swag and event assets   
* `FMC AMER - West/East-Central::Watching`: Issues that the FMC AMER West is tracking   
* `FMC AMER - West/East-Central::Working`: Issues that the FMC AMER West is actively working on   
* `FMC EMEA`: Apply this label for the attention of the Field Marketing Coordinator-EMEA
* `FMC EMEA - Event Tracking`: Event/deadline tracking related to FMC EMEA  

For more information on how Field Marketing utilizes GitLab for agile project management, please visit [Marketing Project Management Guidelines](/handbook/marketing/#-marketing-project-management-guidelines).  

# The Field Marketing Budget
* [Budget Doc](https://docs.google.com/spreadsheets/d/1QC6P0VRWwJheOlGB-9bX8JIF8_4UY3h1cGVT_gacv5M/edit#gid=184318451&range=A1)
Field Marketing manages its budget in a separate doc, that our Finance Business Partners pull into the master marketing forecast. Specific instructions on how we manage our expenses: 
1.	Each FMM will have a qtrly allocation of money to spend based on territory pipeline needs. 
2.	Finance issues will NOT be approved unless all column details are filled in. Field Marketing is the DRI for all columns. 
3.	Every qtr finance will do a pull of this data and push it into the company rolling 4 qtr forecast. Details on date of pull will be forthcoming. 
4.	The "Export" tab will be used to export to the master marketing spreadsheet and should be used by each FMM to ensure they are staying within their expense allocation.
5.	Each expense should have its own row. 
6.	The entire cost of the program is to be forecasted for - Sponsorship + lead scanners + monitor + any other auxiliary cost that the company will incur.
7.	If the event is just 1 day, then the start and end date would be the same. 
8.	The Campaign tag auto populates based off of the program name and the end date (ISO format) of the campaign. The max of 37 characters that can be entered into NetSuite is already taken into consideration in the formula. 
9.	Each tab is protected based on the managers/DRI's for each region. Only those with permissions will be able to edit each tab. Please reach out to the marketing finance business partner if edit access needs to be changed.
10. Only the "FY 21 Spend" column (O) needs to be filled out with the total spend of the program. Equations are in the monthly columns (P-AA) that will evenly spread the total amount over the months that are in the Start and End date columns. However, the equation can be deleted and dollar values can be hard-coded into the cells if the spend is not expected to be evenly spread over the months of the event. 
11.	The MQL target is built off of a formula based on the type of activity the spend is. CXO focused = 70% of the leads will convert. Regional tradeshow = 30% of the leads will convert. ABM = 50% of the leads will convert. 
12.	There will be a swag line item per region per qtr, so each sub region will not need to take this into consideration when it comes to the cost of the activity. AMER: Reminder that the Nadel portal spits out swag costs upon order completion. 
13.	Created Net Suite tags can be found [here](https://docs.google.com/spreadsheets/d/19Le1PrWE1JbqN6Wz3D6tGfy7Ee5PmoetfyLPdwinFV0/edit?ts=5daf2dad#gid=248514497) 
14. At times, expenses are shared across an entire region and we need to account for this shared expense in an overall tab vs. a subregion tab. 
   * An example of this could be run rate swag, stickers, tradeshow assets not tied to a specific event, or on online or in person event that all regions will benefit from.
   * Today, AMER & EMEA have their own regional tabs. (In addition to the subregion tabs each FMM is the DRI for.)
   * The Manager of the region will manage this tab and expense, although reach tactic or event will have its own DRI. 
   * If the amount allocated to the region needs to be changed, the Manager of the regional budget will create an issue in the [Finance project](https://gitlab.com/gitlab-com/finance) for the Financial Analyst to update the total that should be allocated to the overarching tab. That money will be taken out of the other subregional tabs and added into the regional tab, so that the quarterly totals remain aligned to the budget. 

# Legal Approval 
Field Marketing executes contracts with outside vendors to support the work we do. We do so by following the company [procure to pay process](https://about.gitlab.com/handbook/finance/procure-to-pay/#vendor-and-contract-approval-workflow).  

We have an expectation of a 5 business day turnaround for a GitLab executed contract (could be longer depending on the amount of the contract & how many chains of approval the contract needs to go through). 

To help expedite the legal review process, every Wednesday the FMC APAC will ask in the [#fieldmarketing](https://gitlab.slack.com/archives/CCWDAJ8PK) slack channel if there are any outstanding issues that require legal approval. If an issue has been sitting for more than 5 business days and does not have approval from legal, then the FMC is to add the issue in a thread. At the end of the day on Wednesday, the FMC AMER West will ping legal in the issue reminding them to please review. If the FMC APAC or AMER West will be out of the office on Wednesday, it is their responsibility to assign a delegate. 

It should be a rare occasion (and not a result of poor planning), that a contract would need to be urgently (less than 5 business days) turned around. Only after all FMM approvals have happened, should the FMC ping legal on slack asking them to please approve an urgent ask. 

# Field Marketing Communications

## Slack Rooms we use 
* [#fieldmarketing](https://gitlab.slack.com/archives/CCWDAJ8PK) - open to all teams to ask questions to field marketing & where weekly stand-up reports get posted to. 
   * If you're an employee at GitLab trying to reach Field Marketing, please head over to our slack channel.
     * **NOTE**: If you're requesting we sponsor an event, instructions can be found [here](/handbook/marketing/revenue-marketing/field-marketing/#whats-currently-scheduled-in-my-region). 
     * If a region specific question is asked in the [#fieldmarketing](https://gitlab.slack.com/archives/CCWDAJ8PK) slack room, please tag the [regional Field Marketing leader](#whats-currently-scheduled-in-my-region), as that person is the DRI. 
* [#fieldmarketing-fyi](https://gitlab.slack.com/archives/C01502JC5BK) - Official channel for Field Marketing Leadership to post announcements to the Field Marketing team. 
   * Restricted permission levels to only FMM Leadership posting the announcements 
   * Announcements can be submitted to your manager if you would like to something shared 
   * All members have comment access to allow clarifications if needed on a thread 
   * You are to react with an emoji (thumbs-up, eyes…) to signal to managers that you’ve seen an announcement
   * There should be no need to cross-post announcements (such as posting a link from another slack channel to the #fieldmarketing channel)
      * With a #company-fyi and the [#fieldmarketing-fyi](https://gitlab.slack.com/archives/C01502JC5BK), all relevant info should be successfully communicated by managers to team members.  
   * Default to issues first, then public channels when possible to avoid siloing work information to private discussions.  
      * Use DMs for personal, private conversation rather than work communication (unless you are certain it is only relevant to the 2 of you or you need immediate attention).
* #fieldofdreamsteam: private room only assessible to FMMs - funny gifs, private questions for the team. 
* Please keep in mind we have detailed instructions on [how GitLab uses slack](/handbook/communication/#slack). 
## Monthly Calls 

* The World Wide Field Marketing team has one standing call on the calendar. 
* The team meets on the 1st Wednesday of each month.
* All team members are invited to each call although, if the call is scheduled outside of the team member's normal working hours, the team member is NOT expected to attend. The calendar invite simply serves as a reminder to the team member to check out the recording of the meeting the next working day and to also review the agenda. 
* The purpose of the team call is to share relevant company, marketing, and Field Marketing announcements.
* Anyone on the team should feel empowered to add content to the team meeting agenda.
* We will also have a specific `lessons learned` section where each Country Field Marketing Manager will select 1 FMM event or other tactic event recap to review.
    * The FMM who is the DRI for the event or other tactic we will be reviewing as a team will add the `Event Recap` link from the `Event Planning and Recap` to the agenda 
    * We review the recap prior to the call, the FMM DRI briefly gives a run down of the event or other tactic, then we dialogue.
    * Each region will be represented - AMER, APAC, and EMEA - so we will review 3 each call. 
* We also discuss use cases - could be how you've worked with social media, how you built a report in SFDC to help the team be more efficient, etc, 
* Guest Speakers - at times we will also invite other colleagues from the company to address our team as a whole
* As a handbook first company, if you are going to bring a topic to the team, please think twice on if you should add just an agenda item, or if you should add an agenda item that links to an MR or a handbook page. 
* If you're not on the Field Marketing team at GitLab and you're a GitLab employee who's interested in joining our team meeting, please feel free to ask in the #fieldmarketing slack room, we'd welcome the opportunity to host you! 

## Async Status Updates

Since we are a [remote](/company/culture/all-remote/) company, we utilize a Slack plugin called [Geekbot](https://geekbot.io/) to coordinate status updates. Field Marketing world wide currently conducts 1 weekly standup. Geekbot shares this update in the public #fieldmarketing slack room.

## Weekly Status Update
The **Weekly Status Update** is configured to run at 9 AM local time on Monday, and contains a series of rotating questions. Below is an example of questions you may see: 

1. ***What was your favorite part of the weekend?*** 

    The goal with this question is for you to get to know your colleagues and for you to be able to share what excited you from the previous few days when you weren't working.

2. ***What's happening in your life?*** 

    Is there anything you'd like the team to know about what's going in your life? Feel free to share as much or as little as you feel comfortable sharing. 
3. ***Are you traveling anywhere this week? If so, where and why.*** <--- This question is paused for the moment. :) 

    As Field Marketers, we travel up to 50% of the time. Sharing where you are is important, especially if are in a different timezone. 

4. ***What are your top 1-3 priorities for the next week?***

    These top 3 priorities should be focused on what you plan to accomplish that week. 

5. ***Anything blocking your progress?*** 

    Of those 1-3 items listed, do you need any roadblocks removed in order to accomplish the priorities? 
    
You will be notified via the Geekbot plug in on slack at 9 AM your local time on Mondays, as stated above. It is important to note, that unless you answer all questions in the Geekbot plug in, your answers will NOT be shared with your colleagues, so please be sure to complete all questions! 

# Other pages to review for a full understanding of how Field Marketing at GitLab operates
* [Events at GitLab](/handbook/marketing/events/)
* [Marketing Program Management](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/)
* [Marketing Operations](/handbook/marketing/marketing-operations/)
* [Sales Development](/handbook/marketing/revenue-marketing/sdr/)
* [Links GitLab Field Marketers find useful](https://docs.google.com/spreadsheets/d/1gjJghF8Va-G0lYWsDaYXKG7JPtADLS2Jhrh8IVHkizQ/edit?ts=5d249a33#gid=1748424259&range=A1)
* [Field Marketing onboarding videos](https://drive.google.com/open?id=1m8ReMIiymMTqqk5PJAG7u_IG-Q5pkusV) - NOTE - these are also in the Field Marketing Onboarding issue that is kept in the [Marketing onboarding project](https://gitlab.com/gitlab-com/marketing/onboarding#onboarding)

