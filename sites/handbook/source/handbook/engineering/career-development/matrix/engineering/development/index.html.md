---
layout: handbook-page-toc
title: "Development Department Career Framework"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Development Department Career Framework

These are the expected competencies of team members at GitLab by Job Title.
 
1. [Intermediate](/handbook/engineering/career-development/matrix/engineering/development/intermediate)
1. [Senior](/handbook/engineering/career-development/matrix/engineering/developmen/senior)
1. [Staff](/handbook/engineering/career-development/matrix/engineering/developmen/staff)
1. Distinguished
1. Fellow


## Development Sub-departments

* CI/CD
* Defend
* Dev
* Enablement
* Growth
* Ops
* Secure
