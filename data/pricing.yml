plans:
  - id: free
    title: Free
    description: Develop with a team of any size
    monthly_price: 0
    features:
      - Run your continuous integration (CI) pipelines for up to 2,000 minutes (~190 CI builds).
      - Unlimited private and public repositories.
      - Unlimited collaborators in public and private repositories.
      - Organize your issues into Scrum or Kanban boards.
    links:
      hosted: https://gitlab.com/users/sign_up
      self_managed: /install/
  - id: bronze-starter
    title: Bronze / Starter
    description: Control what goes<br/>into production
    monthly_price: 4
    features:
      - Reduce risk by requiring team leaders to approve merge requests.
      - Ensure code quality with multiple code reviewers.
      - Quickly identify development delays with burndown charts.
      - Get help with access to direct support with response guaranteed within 24hrs.
      - Enable push rules to prevent important or secret files from being pushed to a remote repository.
    links:
      hosted: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0ff5a840412015aa3cde86f2ba6
      self_managed: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0ff6145d07001614efff26d15da
  - id: silver-premium
    title: Silver / Premium
    description: Plan across<br/>multiple teams
    monthly_price: 19
    features:
      - Run your continuous integration (CI) pipelines for up to 10,000 minutes (~1900 CI builds).
      - Plan and organize parallel development with multiple issue boards.
      - Report on the productivity of each team in your organization by using issue analytics.
      - Build fully integrated delivery pipelines for your microservice architecture.
      - Monitor the status of your deployments with Deploy Boards.
      - Lower risk by incrementally rolling out your new code.
      - Reduce outage risks with canary deployments.
      - Priority Support
    links:
      hosted: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0fd5a840403015aa6d9ea2c46d6
      self_managed: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0ff6145d07001614f0ca2796525
  - id: gold-ultimate
    title: Gold / Ultimate
    description: Secure & monitor production
    monthly_price: 99
    features:
      - Run your continuous integration (CI) pipelines for up to 50,000 minutes (~9,500 CI builds).
      - Dynamically scan Docker images for vulnerabilities before production pushes.
      - Scan security vulnerabilities, license compliance and dependencies in your CI pipeline.
      - Get alerted when your application performance degrades.
      - Ensure deadlines are met by viewing shared roadmaps for all your development teams.
      - Facilitate efficient communication by adding unlimited (free) read-only users.
    links:
      hosted: https://gitlab.com/-/subscriptions/new?plan_id=2c92a0fc5a83f01d015aa6db83c45aac
      self_managed: https://customers.gitlab.com/subscriptions/new?plan_id=2c92a0fe6145beb901614f137a0f1685
questions:
  - question: Does GitLab offer a money-back guarantee?
    answer: >-
      Yes, we offer a 45-day money-back guarantee for any GitLab self-hosted or GitLab.com plan. See full details on
      refunds in our [terms of service](/terms/)
  - question: What are pipeline minutes?
    answer: >-
      Pipeline minutes are the execution time for your pipelines on our shared runners. Execution on your own runners
      will not increase your pipeline minutes count and is unlimited.
  - question: What happens if I reach my minutes limit?
    answer: >-
      If you reach your limits, you can
      [purchase additional CI minutes](https://customers.gitlab.com/plans) at $8/month for 1000 minutes, or
      upgrade your account to Silver or Gold. Your own runners can still be used even if you reach your limits.
  - question: Does the minute limit apply to all runners?
    answer: >-
      No. We will only restrict your minutes for our shared runners. If you have a
      [specific runner setup for your projects](https://docs.gitlab.com/runner/), there is no limit to your build time
      on GitLab.com.
  - question: Can I acquire a mix of licenses?
    answer: >-
      No, all users in the group need to be on the same plan.
  - question: Are GitLab Pages included in the free plan?
    answer: >-
      Absolutely, GitLab Pages will remain free for everyone.
  - question: Can I import my projects from another provider?
    answer: >-
      Yes. You can import your projects from most of the existing providers, including GitHub and Bitbucket.
      [See our documentation](https://docs.gitlab.com/ee/workflow/importing/README.html) for all your import
      options.
  - question: I already have an account, how do I upgrade?
    answer: >-
      Head over to [https://customers.gitlab.com](https://customers.gitlab.com), choose the plan that is right for you.
  - question: Do plans increase the minutes limit depending on the number of users in that group?
    answer: >-
      No. The limit will be applied to a group, no matter the number of users in that group.
  - question: How much space can I have for my repo on GitLab.com?
    answer: >-
      10GB per project.
  - question: Can I buy additional storage space for myself or my organization?
    answer: >-
      Not yet, but we are [working on it](https://gitlab.com/gitlab-org/gitlab/issues/5634), you will soon be able to
      track your storage usage across all features and buy additional storage space for
      GitLab.com.
  - question: Do you have special pricing for open source projects or educational institutions?
    answer: >-
      Yes! We provide free Gold and Ultimate licenses to qualifying open source projects and educational institutions. Find out more by visiting our [GitLab for Open Source](/solutions/open-source/) and [GitLab for Education](/solutions/education/) program pages.
  - question: Where is GitLab.com hosted?
    answer: >-
      Currently we are hosted on the Google Cloud Platform in the USA
  - question: What features do not apply to GitLab.com?
    answer: |-
      - 24/7 uptime support
      - Access to the server
      - Runs on metal
      - Highly Available setups
      - Run your own software on your instance
      - Use your configuration management software
      - Use standard Unix tools for maintenance and monitoring
      - Single package installation
      - Single configuration file
      - Basic backup and restore mechanism without additional software
      - IPv6 ready
      - AD / LDAP integration
      - Multiple LDAP / AD server support
      - Access to and ability to modify source code
      - Advanced Global Search
      - Advanced Syntax Search
      - Create and remove admins based on an LDAP group
      - Kerberos user authentication
      - Integrate with Atlassian Crowd
      - Multiple LDAP server support (compatible with AD)
      - PostgreSQL HA
      - Import from GitLab.com
      - Email all users of a project, group, or entire server
      - Limit project size at a global, group, and project level
      - Omnibus package supports log forwarding
      - Admin Control
      - Restrict SSH Keys
      - LDAP group sync
      - LDAP group sync filters
      - Live upgrade assistance
      - Audit Logs
      - Auditor users
      - Disaster Recovery
      - DevOps Score
      - Database load balancing for PostgreSQL
      - Mattermost integration
      - Object storage for artifacts
      - Object storage for LFS
      - Globally distributed cloning with GitLab Geo
      - Support for High Availability
      - Containment
      - Control
      - Retrieval
      - Configurable issue closing pattern
      - Custom Git Hooks
      - Various authentication mechanisms
      - Fast SSH Authorization
      - Instant SSL with Let's Encrypt for Omnibus GitLab
      - Plugins
      - Supports geolocation-aware DNS
      - Instance file templates
      - Instance-level Kubernetes cluster configuration
      - Smart card support
      - Instance-level kubernetes clusters
      - Show most affected projects in Group Security Dashboard
      - New configuration screen for Secure
      - Credentials Management
      - Compliance Dashboard
  - question: What is a user?
    answer: >-
      User means each individual end-user (person or machine) of Customer and/or its Affiliates (including, without
      limitation, employees, agents, and consultants thereof) with access to the Licensed Materials hereunder.
  - question: Can I add more users to my subscription?
    answer: >-
      Yes. You have a few options. You can add users to your subscription any time during the subscription period. You
      can log in to your account via the [GitLab Customer Portal](https://customers.gitlab.com) and add more seats or by
      either contacting [renewals@gitlab.com](mailto:renewals@gitlab.com) for a quote. In either case, the cost will be
      prorated from the date of quote/purchase through the end of the subscription period. You may also pay for the
      additional licences per our true-up model.
  - question: The True-Up model seems complicated, can you illustrate?
    answer: >-
      If you have 100 active users today, you should purchase a 100 user subscription. Suppose that when you renew next
      year you have 300 active users (200 extra users). When you renew you pay for a 300 user subscription and you also
      pay the full annual fee for the 200 users that you added during the year.
  - question: How does the license key work?
    answer: >-
      The license key is a static file which, upon uploading, allows GitLab Enterprise Edition to run. During license
      upload we check that the active users on your GitLab Enterprise Edition instance doesn't exceed the new number of
      users. During the licensed period you may add as many users as you want. The license key will expire after one
      year for GitLab subscribers.
  - question: What happens when my subscription is about to expire or has expired?
    answer: >-
      You will receive a new license that you will need to upload to your GitLab instance. This can be done by following
      [these instructions](https://docs.gitlab.com/ee/user/admin_area/license.html).
  - question: What happens if I decide to not renew my subscription?
    answer: >-
      14 days after the end of your subscription, your key will no longer work and GitLab Enterprise Edition will not be
      functional anymore. You will be able to downgrade to GitLab Community Edition, which is free to use.
